//
//  myClinContatoViewController.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 10/07/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import UIKit

class myClinContatoViewController: UIViewController {
  
  @IBOutlet weak var contatoView: UIScrollView!
  
  @IBOutlet weak var backButton: UIButton!
  
  @IBAction func backButtonAction(_ sender: UIButton) {
    
    self.dismiss(animated: true)
  }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
