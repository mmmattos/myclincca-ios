//
//  myClinTipoProcedimentosViewController.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 10/07/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import UIKit

class myClinTipoProcedimentosViewController: UIViewController {
  
  var vcTipo: String!
  
  @IBOutlet weak var titleButtonLabel: UIButton!
  
  @IBOutlet weak var tipoProcedimentosView: UIScrollView!
  
  @IBOutlet weak var backButton: UIButton!
  
  @IBAction func backButtonAction(_ sender: UIButton) {
    self.dismiss(animated: true)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    
    self.titleButtonLabel.setTitle(self.vcTipo, for: .normal)
    
    tipoProcedimentosView.isScrollEnabled = true
    
    loadTipoProcedimento() {
      (item,  error) in
      if let tp = item {
        Utilities.Log(
          message: "Found TipoProcedimento To Render: \(tp)",
          #file,
          Utilities.typeName(some: self),
          #function,
          #line
        )
        self.render(tipoProcedimento: tp)
      }
    }
    
  }
  
  func render( tipoProcedimento: TipoProcedimento?) {
    
    var sectionY = 20
    var inativos = 0

    guard let tp = tipoProcedimento else {
      createCustomLabel(
        text: "Nenhuma informação disponível para o tipo de procedimento escolhido.",
        tipo: "mansagem",
        positionY: Double(sectionY),
        nbrLines: 5,
        fontName: "HelveticaNeue-CondensedBold",
        fontSize: 20
      )
      return
    }
    
    if !tp.ativo {
      createCustomLabel(
        text: "O tipo de Procedimento escolhido não está disponível!",
        tipo: "mansagem",
        positionY: Double(sectionY),
        nbrLines: 5,
        fontName: "HelveticaNeue-CondensedBold",
        fontSize: 20
      )
      return
    }
    
    Utilities.Log(
      message: "Found Tipo Procedimento To Render: \(tp)",
      #file,
      Utilities.typeName(some: self),
      #function,
      #line
    )
    
    if tp.descricao != nil {
      let nbrLines = tp.descricao.characters.count / 25
      createCustomLabel(
        text: tp.descricao!,
        tipo: "texto",
        positionY: Double(sectionY),
        nbrLines: nbrLines,
        fontName: "HelveticaNeue-CondensedBlack",
        fontSize: 20
      )
      sectionY = sectionY + 40
    }
    

    print ("Full Height is: \(self.fullHeight)")
    let screenSizeWidth = UIScreen.main.bounds.size.width
    tipoProcedimentosView.contentSize =
      CGSize(width: screenSizeWidth, height: CGFloat(self.fullHeight+950))
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  
  func loadTipoProcedimento(
    completionHandler: @escaping (TipoProcedimento?, NSMutableDictionary?) -> ()
    ) -> () {
    
    // MARK: - Read Tipo Procedimento
    ApiController.doGetTipoProcedimentosByTipo(tipo: self.vcTipo)
      {
        (json, error) in
        if let err = error {
          
          Utilities.Log(
            message: "Error reading Tipo Procedimentos! \(err)",
            #file,
            Utilities.typeName(some: self),
            #function,
            #line
          )
          
          let message = "Erro na leitura dos Tipos de Procedimentos. Por favor tente mais tarde!"
          mmmattosAlerts.showAlert(
            viewController:self as UIViewController,
            style: .alert,
            message: message,
            completion: nil,
            buttons: (.default, "Ok", nil)
          )
          return
        }
        guard let duvs = json else {
          
          let message = "Não há Tipos de Procedimentos disponíveis. Por favor tente mais tarde!"
          
          mmmattosAlerts.showAlert(
            viewController:self as UIViewController,
            style: .alert,
            message: message,
            completion: nil,
            buttons: (.default, "Ok", nil)
          )
          return
        }
        completionHandler(duvs, nil)
        
    }
  }

  var fullHeight: Double = 0
  
  func createCustomLabel(text: String, tipo: String, positionY: Double, nbrLines: Int, fontName: String, fontSize: Double) {
    
    let initialFrame = CGRect(x: CGFloat(0), y: CGFloat(positionY), width: tipoProcedimentosView.frame.width , height: CGFloat(44))
    let contentInsets = UIEdgeInsets(top: 20, left: 20, bottom: 10, right: 10)
    let paddedFrame = UIEdgeInsetsInsetRect(initialFrame, contentInsets)
    let codedLabel:UILabel = UILabel(frame: paddedFrame)
    codedLabel.numberOfLines=nbrLines
    codedLabel.contentMode = .center
    switch tipo.lowercased() {
    case "texto" :
      codedLabel.textAlignment = .left
      codedLabel.textColor=UIColor.darkGray
      codedLabel.backgroundColor = UIColor.clear
    case "mensagem" :
      codedLabel.textAlignment = .center
      codedLabel.textColor=UIColor.darkGray
      codedLabel.backgroundColor = UIColor.clear
    default :
      codedLabel.textAlignment = .center
      
    }
    
    codedLabel.text = text
    
    codedLabel.font=UIFont.init(name: fontName, size: CGFloat(fontSize))
    // codedLabel.backgroundColor=UIColor.clear
    codedLabel.sizeToFit()
    self.tipoProcedimentosView.addSubview(codedLabel)
    
    print ("Rendered Label: \(String(describing: codedLabel.text))")
    
    fullHeight = fullHeight + Double(codedLabel.frame.height)
    
    
  }
  
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}
