//
//  HorarioAgenda.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 25/04/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import Foundation
class HorarioAgenda : Serializable {
  var data:   String
  var horario: String
  var emailAutor: String
  var ativo: Bool?
  var disponivel: Bool?
  var autorizado: Bool?
  var pacienteNome: String?
  var pacienteTelefone: String?
  var pacienteEmail: String?
  var pacienteNasc: String?
  var procedimentoMedHonorarios: String?
  var procedimentoAnatomoObs: String?
  var procedimento: String?
  var user: String?
  var id: String?
  
  init(
    data: String,
    horario: String,
    emailAutor: String,
    ativo: Bool = false,
    disponivel: Bool = false,
    autorizado: Bool = false,
    pacienteNome: String = "",
    pacienteTelefone: String = "",
    pacienteEmail: String = "",
    pacienteNasc: String = "",
    procedimentoMedHonorarios: String = "",
    procedimentoAnatomoObs: String = "",
    procedimento: String? = nil,
    user: String? = nil,
    id: String? = nil
    ) {
    
    self.id = nil
    self.data = data
    self.horario = horario
    
    self.emailAutor = emailAutor
    self.ativo = ativo
    self.disponivel = disponivel
    self.autorizado = autorizado
    
    self.pacienteNome = pacienteNome
    self.pacienteTelefone = pacienteTelefone
    self.pacienteEmail = pacienteEmail
    self.pacienteNasc = pacienteNasc
    self.procedimentoMedHonorarios = procedimentoMedHonorarios
    self.procedimentoAnatomoObs = procedimentoAnatomoObs
    
    self.procedimento = procedimento
    self.user = user
    self.id = id
    
  }
}
