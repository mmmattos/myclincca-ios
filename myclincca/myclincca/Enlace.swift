//
//  Enlace.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 11/07/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import Foundation
class Enlace: Serializable {
  var url: String!
  var titulo: String!
  var ativo: Bool
  
  init(
    urlParam: String,
    tituloParam: String,
    ativoParam: Bool
    ) {
    self.url = urlParam
    self.titulo = tituloParam
    self.ativo = ativoParam
  }
}
