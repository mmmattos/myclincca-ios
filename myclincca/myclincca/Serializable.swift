//
//  Serializable.swift
//  Enceen.iOS
//
//  Created by Miguel Miranda de Mattos.
//  
//
/*
Converts A class to a dictionary, used for serializing dictionaries to JSON
Supported objects:
- Serializable derived classes (sub classes)
- Arrays of Serializable
- NSData
- String, Numeric, and all other NSJSONSerialization supported objects
*/

import Foundation



public class Serializable: NSObject {
    private class SortedDictionary : NSMutableDictionary {
        private var  dictionary = [String: AnyObject]()
        
        override var count : Int {
            return dictionary.count
        }
        
        override func keyEnumerator() -> NSEnumerator {
            let sortedKeys : NSArray = dictionary.keys.sorted() as NSArray
            return sortedKeys.objectEnumerator()
        }
        
        override func setValue(_ value: Any?, forKey key: String) {
            dictionary[key] = value as AnyObject?
        }
        
        
        override func object(forKey aKey: Any) -> Any? {
            return dictionary[aKey as! String]
        }
    }
    
    
    public func formatKey(key: String) -> String {
        return key
    }
    
    public func formatValue(value: AnyObject?, forKey: String) -> AnyObject? {
        return value
    }
    
    func setValue(dictionary: NSDictionary, value: AnyObject?, forKey: String) {
        dictionary.setValue(formatValue(value: value, forKey: forKey), forKey: formatKey(key: forKey))
    }
    
    /**
     Converts the class to a dictionary.
     - returns: The class as an NSDictionary.
     */
    public func toDictionary() -> NSDictionary {
        let propertiesDictionary = SortedDictionary()
        let mirror = Mirror(reflecting: self)
        for (propName, propValue) in mirror.children {
            if let propValue: AnyObject = self.unwrap(any: propValue) as? AnyObject, let propName = propName {
                if let serializablePropValue = propValue as? Serializable {
                    setValue(dictionary: propertiesDictionary, value: serializablePropValue.toDictionary(), forKey: propName)
                } else if let arrayPropValue = propValue as? [Serializable] {
                    var subArray = [NSDictionary]()
                    for item in arrayPropValue {
                        subArray.append(item.toDictionary())
                    }
                    setValue(dictionary: propertiesDictionary, value: subArray as AnyObject?, forKey: propName)
                } else if propValue is Int || propValue is Double || propValue is Float {
                    setValue(dictionary: propertiesDictionary, value: propValue, forKey: propName)
                //} else if let dataPropValue = propValue as? NSData {
                //    setValue(dictionary: propertiesDictionary, value: dataPropValue.base64EncodedString(options: .Encoding64CharacterLineLength), forKey: propName)
                } else if let datePropValue = propValue as? NSDate {
                    setValue(dictionary: propertiesDictionary, value: datePropValue.timeIntervalSince1970 as AnyObject?, forKey: propName)
                } else if let boolPropValue = propValue as? Bool {
                    setValue(dictionary: propertiesDictionary, value: boolPropValue as AnyObject?, forKey: propName)
                } else {
                    setValue(dictionary: propertiesDictionary, value: propValue, forKey: propName)
                }
            }
            else if let propValue:Int8 = propValue as? Int8 {
                setValue(dictionary: propertiesDictionary, value: NSNumber(value: propValue), forKey: propName!)
            }
            else if let propValue:Int16 = propValue as? Int16 {
                setValue(dictionary: propertiesDictionary, value: NSNumber(value: propValue), forKey: propName!)
            }
            else if let propValue:Int32 = propValue as? Int32 {
                setValue(dictionary: propertiesDictionary, value: NSNumber(value: propValue), forKey: propName!)
            }
            else if let propValue:Int64 = propValue as? Int64 {
                setValue(dictionary: propertiesDictionary, value: NSNumber(value: propValue), forKey: propName!)
            }
            else if let propValue:UInt8 = propValue as? UInt8 {
                setValue(dictionary: propertiesDictionary, value: NSNumber(value: propValue), forKey: propName!)
            }
            else if let propValue:UInt16 = propValue as? UInt16 {
                setValue(dictionary: propertiesDictionary, value: NSNumber(value: propValue), forKey: propName!)
            }
            else if let propValue:UInt32 = propValue as? UInt32 {
                setValue(dictionary: propertiesDictionary, value: NSNumber(value: propValue), forKey: propName!)
            }
            else if let propValue:UInt64 = propValue as? UInt64 {
                setValue(dictionary: propertiesDictionary, value: NSNumber(value: propValue), forKey: propName!)
            }
        }
        
        return propertiesDictionary
    }
    
    /**
     Converts the class to JSON.
     - returns: The class as JSON, wrapped in NSData.
     */
    public func toJson(prettyPrinted : Bool = false) -> NSData? {
        let dictionary = self.toDictionary()
        
        if JSONSerialization.isValidJSONObject(dictionary) {
            do {
                let json = try JSONSerialization.data(withJSONObject: dictionary, options: (prettyPrinted ? .prettyPrinted : JSONSerialization.WritingOptions()))
                return json as NSData?
            } catch let error as NSError {
                print("ERROR: Unable to serialize json, error: \(error)")
                NotificationCenter.default.post(
                    name: NSNotification.Name(rawValue: "CrashlyticsLogNotification"),
                    object: self)
            }
        }
        
        return nil
    }
    
    /**
     Converts the class to a JSON string.
     - returns: The class as a JSON string.
     */
    public func toJsonString(prettyPrinted : Bool = false) -> String? {
        if let jsonData = self.toJson(prettyPrinted: prettyPrinted) {
            return NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as String?
        }
        
        return nil
    }
    
    
    /**
     Unwraps 'any' object. S
     - swift 3 compatible.
     - returns: The unwrapped object.
     */
    func unwrap(any:Any) -> Any? {
      let mi = Mirror(reflecting: any)
      
      if mi.displayStyle != .optional { return any }
      if mi.children.count == 0 { return nil }
      
      let (_, some) = mi.children.first!
      return some
    }
}
