//
//  Horarios.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 18/04/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import Foundation


struct Horario {
  
  let start: TimeInterval
  let end: TimeInterval
  let interval: TimeInterval
  
  init(start: TimeInterval, interval: TimeInterval, end: TimeInterval) {
    self.start = start
    self.interval = interval
    self.end = end
  }
  
  init(startHour: TimeInterval, intervalMinutes: TimeInterval, endHour: TimeInterval) {
    self.start = startHour * 60 * 60
    self.end = endHour * 60 * 60
    self.interval = intervalMinutes * 60
  }
  
  var timeRepresentations: [String] {
    let dateComponentFormatter = DateComponentsFormatter()
    dateComponentFormatter.unitsStyle = .positional
    dateComponentFormatter.allowedUnits = [.minute, .hour]
    
    let dateComponent = NSDateComponents()
    return timeIntervals.map { timeInterval in
      dateComponent.second = Int(timeInterval)
      return dateComponentFormatter.string(from: dateComponent as DateComponents)!
    }
  }
  
  var timeIntervals: [TimeInterval]{
    return Array(stride(from: start, through: end, by: interval))
  }
}

