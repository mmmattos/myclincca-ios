//
//  Depoimento.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 13/07/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import Foundation

class Depoimento: Serializable {
  var textoDepoimento: String!
  var emailAutor: String!
  var ativo: Bool
  
  init(
    textoDepoimentoParam: String,
    emailAutorParam: String,
    ativoParam: Bool
    ) {
      self.textoDepoimento = textoDepoimentoParam
      self.emailAutor = emailAutorParam
      self.ativo = ativoParam
  }
}
