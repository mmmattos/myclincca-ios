//
//  myClinRegisterViewController.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 02/03/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import Foundation
import UIKit
import KeychainAccess


class RegisterViewController:
  UIViewController
  , UITextFieldDelegate
  , UITextViewDelegate
, UIScrollViewDelegate {
  
  var activeTextField = UITextField()
  let placeholderPW = "Informe sua senha aqui."
  let placeholderEmail = "Informe aqui o seu endereço de email."
  let dontHaveAnAccount = "http://www.myclincca.com.br/signup"
  
  private let SCREEN_SIZE = UIScreen.main.bounds
  private let REGISTER_BOX_HEIGHT_FACTOR:CGFloat = 667.0/230.0
  private let REGISTER_BOX_MIN_HEIGHT:CGFloat = 320.0
  
  @IBOutlet weak var logoImg: UIImageView!
  
  @IBOutlet var registerBox: UIView!
  @IBOutlet var scrollView: UIScrollView!
  
  @IBOutlet var emailLabel: UILabel!
  @IBOutlet var emailTextBox: UITextField!
  @IBOutlet var pwLabel: UILabel!
  @IBOutlet var pwTextBox: UITextField!
  @IBOutlet weak var nomeLabel: UILabel!
  
  @IBOutlet weak var nomeTextBox: UITextField!
  @IBOutlet var termaLabel: UILabel!
  
  @IBOutlet weak var repitaPwLabel: UILabel!
  
  @IBOutlet var txtDontHaveAnAccount: UITextView!
  @IBOutlet weak var repitaPwTextBox: UITextField!
  
  @IBOutlet weak var especialidadeLabel: UILabel!
  @IBOutlet weak var especialidadeTextBox: UITextField!
  
  @IBOutlet weak var cremersLabel: UILabel!
  @IBOutlet weak var cremersTextBox: UITextField!
  
  @IBOutlet weak var registerButton: UIButton!
  
  @IBOutlet weak var backButton: UIButton!
  
  // Temp store for the second
  var userCpf: String!
  var userRg: String!
  var userEstadoCivil: String!
  var userTelefone: String!
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    
    registerBox.layer.cornerRadius = 16
    registerBox.layer.masksToBounds = true  // optional
    var desiredHeight :CGFloat = SCREEN_SIZE.height / REGISTER_BOX_HEIGHT_FACTOR
    desiredHeight = (desiredHeight < REGISTER_BOX_MIN_HEIGHT ? REGISTER_BOX_MIN_HEIGHT : desiredHeight)
    let boxY = logoImg.frame.size.height + 30.0
    registerBox.frame.origin.y = boxY
    registerBox.frame.size.height = desiredHeight
    
    //let heightOfSubView = SCREEN_SIZE.height / 2 - SCREEN_SIZE.height * GAP_BETWEEN_VIEWS/2
    //let widthOfSubView = SCREEN_SIZE.width / 2 - SCREEN_SIZE.height * GAP_BETWEEN_VIEWS/2
    
    emailTextBox.text = placeholderEmail
    emailTextBox.textAlignment = NSTextAlignment.center
    emailTextBox.textColor = UIColor.gray
    emailTextBox.layoutMargins = UIEdgeInsets.init(top: 0.0,left: 0.0,bottom: 0.0,right: 0.0)
    emailTextBox.tag = 3
    emailTextBox.delegate = self
    
    
    // This field will always show the placeholder initially.
    // Dont need to garble text at this moment.
    //
    pwTextBox.text = placeholderPW
    pwTextBox.isSecureTextEntry = false
    pwTextBox.textAlignment = NSTextAlignment.center
    pwTextBox.textColor = UIColor.gray
    pwTextBox.layoutMargins = UIEdgeInsets.init(top: 0.0,left: 0.0,bottom: 0.0,right: 0.0)
    pwTextBox.tag = 4
    self.pwTextBox.delegate = self
    
    
    self.cremersTextBox.delegate = self
    self.especialidadeTextBox.delegate = self
    self.repitaPwTextBox.delegate = self
    self.nomeTextBox.delegate = self
    
  }
  
  
  override func viewDidLayoutSubviews() {
    self.scrollView.contentSize = self.view.frame.size
  }
  
  func cleanUpTempRegisterStore() {
    let tempRegStore = UserDefaults.standard
    tempRegStore.removeObject(forKey: "register.nome")
    tempRegStore.removeObject(forKey: "register.email")
    tempRegStore.removeObject(forKey: "register.pw")
    tempRegStore.removeObject(forKey: "register.especialidade")
    tempRegStore.removeObject(forKey: "register.cremers")
    tempRegStore.removeObject(forKey: "register.cpf")
    tempRegStore.removeObject(forKey: "register.rg")
    tempRegStore.removeObject(forKey: "register.telefone")
    tempRegStore.removeObject(forKey: "register.estado-civil")
    tempRegStore.removeObject(forKey: "register.endereco")
    tempRegStore.removeObject(forKey: "register.cidade")
    tempRegStore.removeObject(forKey: "register.estado")
  }
  
  
  
  @IBAction func backButtonAction(_ sender: UIButton) {
    cleanUpTempRegisterStore()
    self.dismiss(animated: true)
  }
  

  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(true)

    let keychain = Keychain(service: "com.myclincca.myclincca-token")
    
    Utilities.Log (
      message:"Try to get session token from keychain.",
      #file,
      Utilities.typeName(some:self),
      #function,
      #line)
    
    // Get session from keychain.
    do {
      if try keychain.contains(Constants.KeychainKeys.SessionTokenKey) {
        let session_token = try keychain.get(Constants.KeychainKeys.SessionTokenKey)
        if session_token != nil {
          Utilities.Log (
            message:"Got session token: \(session_token)",
            #file,
            Utilities.typeName(some:self),
            #function,
            #line)
          
          let appDelegate = UIApplication.shared.delegate! as! AppDelegate
          let initialViewController = self.storyboard!.instantiateViewController(
                withIdentifier: "myclinccaMenu")
          appDelegate.window?.rootViewController = initialViewController
          appDelegate.window?.makeKeyAndVisible()
          
          return
        }
      }
    }
    catch let error {
      Utilities.Log(
        message: "\(error) ",
        #file,
        Utilities.typeName(some: self),
        #function,
        #line)
    }
    
  }
  
  
  
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    
    self.activeTextField = textField
    
    
    if textField.tag == 3 || textField.tag == 6 {
      textField.text = (textField.text == placeholderEmail ? "" : textField.text)
      textField.textColor = UIColor.black
      textField.textAlignment = NSTextAlignment.center
      
    }
    
    if textField.tag == 4 {
      textField.isSecureTextEntry = (textField.text == placeholderPW ? true : false)
      textField.text = (textField.text == placeholderPW ? "" : textField.text)
      textField.textColor = UIColor.black
      textField.textAlignment = NSTextAlignment.center
      textField.isSecureTextEntry = true
      
    }
    
  }
  
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    self.activeTextField = textField
    
    if textField.tag == 3 && textField.text == "" {
      
      textField.text = placeholderEmail
      textField.textColor = UIColor.lightGray
      textField.textAlignment = NSTextAlignment.center
      
    }
    
    if textField.tag == 4 && textField.text == "" {
      textField.text = placeholderPW
      textField.isSecureTextEntry = false
      textField.textColor = UIColor.lightGray
      textField.textAlignment = NSTextAlignment.center
    }
  }
  
  
  // MARK: - Close Keyboard on Return key.
  func textFieldShouldReturn(_ userText: UITextField) -> Bool {
    userText.resignFirstResponder()
    return true
  }
  
  /**
   Clear input fields.
   */
  func clearFields()
  {
    emailTextBox.text = "";
    pwTextBox.text = "";
    return;
    
  }
  
  
  
  
  //    @IBAction func alreadyUserButtonTapped(sender: Any) {
  //        self.dismiss(animated: true, completion: nil)
  //        return
  //    }
  
  @IBAction func registerStepOneAction(_ sender: Any) {
    
    let tempRegStore = UserDefaults.standard
    
    // VALIDATE
    let userName = (nomeTextBox.text ?? "")
    let userEmail = (emailTextBox.text ?? "")
    let userPassword = (pwTextBox.text ?? "")
    let userPwRepita = (repitaPwTextBox.text ?? "")
    let userNome = (nomeTextBox.text ?? "")
    let userEspecialidade = (especialidadeTextBox.text ?? "")
    let userCremers = (cremersTextBox.text ?? "")
    
    
    
    //Validate fields
    if (userEmail.isEmpty || userPassword.isEmpty ||
      userPwRepita.isEmpty || userNome.isEmpty || userName.isEmpty ||
      userEspecialidade.isEmpty || userCremers.isEmpty)
    {
      mmmattosAlerts.showAlert(
        viewController: self as UIViewController,
        style: .alert,
        message: "Todos os campos são obrigatórios!",
        completion: nil,
        buttons: (.default, "Ok", nil)
      )
      return;
    }
    
    tempRegStore.set(self.nomeTextBox.text, forKey: "register.nome")
    tempRegStore.set(self.emailTextBox.text, forKey: "register.email")
    tempRegStore.set(self.pwTextBox.text, forKey: "register.pw")
    tempRegStore.set(self.especialidadeTextBox.text, forKey: "register.especialidade")
    tempRegStore.set(self.cremersTextBox.text, forKey: "register.cremers")
    
    //Perform segue to RegisterStep1VC.
    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    let vc : RegisterStep1ViewController = mainStoryboard.instantiateViewController(
      withIdentifier: "RegisterStep1VC") as! RegisterStep1ViewController
    self.present(vc, animated: true, completion: nil)
  }
  
  func resetRegisterFields() {
    self.nomeTextBox.text = ""
    self.emailTextBox.text = ""
    self.pwTextBox.text = ""
    self.repitaPwTextBox.text = ""
    self.especialidadeTextBox.text = ""
    self.cremersTextBox.text = ""
    
    
    self.navigationController?.popViewController(animated: true)
    self.dismiss(animated: true, completion: nil)
  }
  
  
  
  override func viewWillAppear(_ animated: Bool) {
    self.startKeyboardObserver()
    
    let tempRegStore = UserDefaults.standard
    
    // If there is TEMP data in UserDefaults, load it.
    
    if let email = tempRegStore.string(forKey: "register.email") {
      self.emailTextBox.text = email
    }
    if let nome = tempRegStore.string(forKey: "register.nome") {
      self.nomeTextBox.text = nome
    }
    if let pw = tempRegStore.string(forKey: "register.pw") {
      self.pwTextBox.isSecureTextEntry = true
      self.repitaPwTextBox.isSecureTextEntry = true
      self.pwTextBox.text = pw
      self.repitaPwTextBox.text = pw
    }
    if let especialidade = tempRegStore.string(forKey: "register.especialidade") {
      self.especialidadeTextBox.text = especialidade
    }
    if let cremers = tempRegStore.string(forKey: "register.cremers") {
      self.cremersTextBox.text = cremers
    }

  }
  
  override func viewWillDisappear(_ animated: Bool) {
    self.stopKeyboardObserver()
  }
  
  private func startKeyboardObserver(){
    NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
  }
  
  private func stopKeyboardObserver() {
    NotificationCenter.default.removeObserver(
      self, name: NSNotification.Name.UIKeyboardWillShow, object: nil
    )
    NotificationCenter.default.removeObserver(
      self, name: NSNotification.Name.UIKeyboardWillHide, object: nil
    )
  }
  
  func keyboardWillShow(notification: NSNotification) {
    guard let info:[AnyHashable:Any] = notification.userInfo,
      let keyboardSize:CGSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size else { return }
    
    let insets:UIEdgeInsets = UIEdgeInsets(top: self.scrollView.contentInset.top, left: 0.0, bottom: keyboardSize.height, right: 0.0)
    
    self.scrollView.contentInset = insets
    self.scrollView.scrollIndicatorInsets = insets
  }
  
  func keyboardWillHide(notification: NSNotification) {
    //guard let info:[AnyHashable:Any] = notification.userInfo,
    //let keyboardSize:CGSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size else { return }
    
    let insets:UIEdgeInsets = UIEdgeInsets.zero;
    //UIEdgeInsets(top: self.scrollView.contentInset.top, left: 0.0, bottom: keyboardSize.height, right: 0.0)
    
    self.scrollView.contentInset = insets
    self.scrollView.scrollIndicatorInsets = insets
  }
  
  
  func translateRegisterErrorCode(status :Int, message: String) -> String {
    var finalMessage = "Register Error. "
    print ("MESSAGE: \(message) - \(status)")
    
    switch status {
    case 404:
      if message == "GOTNOSESSIONINFOFROMCDB" {
        finalMessage = finalMessage + "Não foi possível obter o token para o Aplicativo."
      } else {
        finalMessage = finalMessage + "Este usuário não tem uma conta! Cadastre-se e crie uma nova conta!"
      }
      break;
    case 401:
      if message != "" {
        finalMessage = finalMessage + message
      } else {
        finalMessage = finalMessage + "Email ou senha inválidos. Por favor, tente novamente!"
      }
      break;
    case 400:
      if message == "ERRORGETTINGTOKENFROMCDB" {
        finalMessage = finalMessage + "Não foi possível recuperar a sessão do usuarion. Por favor, tente novamente mais tade!"
      } else {
        finalMessage = finalMessage + "Por favor, tente novamente."
      }
      break;
    case 500:
      finalMessage = finalMessage + "Um erro não identificado foi detectado. Por favor, tente novamente."
      break;
    default:
      finalMessage = finalMessage + "Por favor, tente novamente."
      break;
      
    }
    return finalMessage
  }
}
