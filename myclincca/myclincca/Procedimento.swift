//
//  Procedimento.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 08/05/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import Foundation
class Procedimento : Serializable {
  var procedimento:   String
  var descricao: String
  var duracao: Double
  var tipo: String
  var detalhes: [String]
  var valor: String
  var ativo: Bool
  var data: Date?
  var emailAutor: String?
  var id: String?
  
  init(
    procedimento:   String,
    descricao: String,
    duracao: Double,
    tipo: String,
    detalhes: [String],
    valor: String,
    ativo: Bool,
    data: Date? = nil,
    emailAutor: String? = nil,
    id: String?    ) {
    
    self.procedimento = procedimento
    self.descricao = descricao
    self.duracao = duracao
    self.ativo = ativo
    self.tipo = tipo
    self.valor = valor
    self.detalhes = detalhes
    self.id = id
    if data != nil {
      self.data = data!
    }
    if emailAutor != nil {
      self.emailAutor = emailAutor!
    }
  }
}
