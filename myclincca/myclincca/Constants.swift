//
//  Constants.swift
//
//  Created by Miguel Miranda de Mattos on 8/7/16.
//  Copyright © 2016 Miguel Miranda de Mattos. All rights reserved.
//

import Foundation

struct Constants {
  
  struct AppUrls {

    static let ApiFinalUrl = "http://myclincca.com.br:3000/api"
    static let AuthApiFinalUrl = "http://myclincca.com.br:9000"
    static let BackendFinalUrl = "http://myclincca.com.br:9000"
    
    static let ApiLocalUrl = "http://localhost:3000/api"
    static let AuthApiLocalUrl = "http://localhost:9000"
    static let BackendLocalUrl = "http://localhost:9000"
    
    static let ApiCodioUrl = "http://harlem-market.codio.io:3000/api"
    static let AuthApiCodioUrl = "http://harlem-market.codio.io:9000"
    static let BackendCodioUrl = "http://harlem-market.codio.io:9000"
    
    static let ApiUrl = ApiLocalUrl
    static let AuthApiUrl = AuthApiLocalUrl
    static let BackendUrl = BackendLocalUrl
    
  }
  
  struct Messages {
    static let UserNotIdentified = "Não registrado"
  }
  
  struct KeychainKeys {
    
    static let KeychainService = "com.myclincca.myclincca-token"
    static let SessionTokenKey = "myclincca-api-token"
    static let SessionUserName = "myclincca-user-name"
    static let SessionUserEmail = "myclincca-user-email"
    static let SessionUserId = "myclincca-user-id"
    
  }
  
  struct UserDefaultKeys {
    static let HasRunB4Key = "myclinccaHasRunBefore"
  }
  
  
  struct ClinicaSection {
    struct Props {
      static let secaoId = "secaoId"
      static let secao = "secao"
      static let tipo = "tipo"
      static let conteudo = "conteudo"
      static let ordem = "ordem"
      static let emailAutor = "emailAutor"
      static let ativo = "ativo"
    }
  }
  
  struct Enlaces {
    struct Props {
      static let titulo = "titulo"
      static let url = "url"
      static let ativo = "ativo"
    }
  }
  
  struct Duvidas {
    struct Props {
      static let pergunta = "duvida"
      static let resposta = "resposta"
      static let ativo = "ativo"
    }
  }
  
  struct Depoimentos {
    struct Props {
      static let textoDepoimento = "depoimento"
      static let emailAutor = "emailAutor"
      static let ativo = "ativo"
    }
  }
  
  struct TipoProcedimentos {
    struct Props {
      static let tipo = "tipo"
      static let descricao = "descricao"
      static let emailAutor = "emailAutor"
      static let ativo = "ativo"
    }
  }

}
