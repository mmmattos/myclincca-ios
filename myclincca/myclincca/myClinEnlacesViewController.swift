//
//  myClinEnlacesViewController.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 10/07/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import UIKit

class myClinEnlacesViewController: UIViewController {
  
  var urls: [String] = []

  
  @IBOutlet weak var enlacesView: UIScrollView!
  
  @IBOutlet weak var backButton: UIButton!
  
  @IBAction func backButtonAction(_ sender: UIButton) {
    
    self.dismiss(animated: true)
  }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      
      enlacesView.isScrollEnabled = true
      
      loadEnlaces() {
        (items,  error) in
        if let enlaces = items {
          Utilities.Log(
            message: "Found Enlaces To Render: \(enlaces)",
            #file,
            Utilities.typeName(some: self),
            #function,
            #line
          )
          self.renderEnlaces(enlacesToRender: enlaces)
        }
      }

    }

  
  
  func tapFunction(sender:UITapGestureRecognizer) {
    print("tap working")
    let url = URL(string: urls[(sender.view?.tag)!])
    if #available(iOS 10.0, *) {
      UIApplication.shared.open(url!)
    } else {
      UIApplication.shared.openURL(url!)
    }
  }
  
  var fullHeight: Double = 0
  
  func createCustomLabel(text: String, tipo: String, positionY: Double, nbrLines: Int, fontName: String, fontSize: Double, tag: Int ) {
    let codedLabel:UILabel = UILabel()
    codedLabel.frame = CGRect(x: CGFloat(10), y: CGFloat(positionY), width: enlacesView.frame.width - 20 , height: CGFloat(30))
    switch tipo.lowercased() {
    case "texto" :
      codedLabel.textAlignment = .justified
      codedLabel.textColor=UIColor.darkGray
    case "url" :
      codedLabel.textAlignment = .center
      codedLabel.textColor = UIColor.blue
      let tap = UITapGestureRecognizer(target: self, action: #selector(myClinEnlacesViewController.tapFunction))
      codedLabel.isUserInteractionEnabled = true
      codedLabel.addGestureRecognizer(tap)
      
    default :
      codedLabel.textAlignment = .center
      
    }
    
    codedLabel.text = text
    codedLabel.tag = tag
    codedLabel.numberOfLines=nbrLines
    codedLabel.font=UIFont.init(name: fontName, size: CGFloat(fontSize))
    codedLabel.backgroundColor=UIColor.clear
    //codedLabel.sizeToFit()
    self.enlacesView.addSubview(codedLabel)
    
    //// Play with AutoConstraints...
    // codedLabel.translatesAutoresizingMaskIntoConstraints = false
    // codedLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
    // codedLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
    // codedLabel.centerXAnchor.constraint(equalTo: codedLabel.superview!.centerXAnchor).isActive = true
    // codedLabel.centerYAnchor.constraint(equalTo: codedLabel.superview!.centerYAnchor).isActive = true
    
    print ("Rendered Label: \(String(describing: codedLabel.text))")
    
    fullHeight = fullHeight + Double(codedLabel.frame.height)
    
    
  }
  
  
  func renderEnlaces( enlacesToRender: [Enlace]) {
    
    var sectionY = 20
    for x:Int in 0..<enlacesToRender.count {
      
      let enlace = enlacesToRender[x]
      
      if !enlace.ativo {
        continue
      }
      
      Utilities.Log(
        message: "Found Enlace To Render: \(enlace)",
        #file,
        Utilities.typeName(some: self),
        #function,
        #line
      )
      
      if enlace.titulo != nil {
        createCustomLabel(
          text: enlace.titulo!,
          tipo: "texto",
          positionY: Double(sectionY),
          nbrLines: 2,
          fontName: "HelveticaNeue-CondensedBlack",
          fontSize: 18,
          tag: x
        )
        sectionY = sectionY + 40
      }
      
      if enlace.url != nil {
        urls.append(enlace.url)
        createCustomLabel(
          text: enlace.url!,
          tipo: "url",
          positionY: Double(sectionY),
          nbrLines: 2,
          fontName: "HelveticaNeue-CondensedBlack",
          fontSize: 18,
          tag: x
        )
        sectionY = sectionY + 40
      }
    }
    print ("Full Height is: \(self.fullHeight)")
    let screenSizeWidth = UIScreen.main.bounds.size.width
    enlacesView.contentSize = CGSize(width: screenSizeWidth, height: CGFloat(self.fullHeight+950))
  }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
  
  
  func loadEnlaces(
    completionHandler: @escaping ([Enlace]?, NSMutableDictionary?) -> ()
    ) -> () {
    
    // MARK: - Read the Clinica Sections
    ApiController.doGetEnlaces()
      {
        (json, error) in
        if let err = error {
          
          Utilities.Log(
            message: "Error reading Enlaces! \(err)",
            #file,
            Utilities.typeName(some: self),
            #function,
            #line
          )
          
          let message = "Erro na leitura dos Links. Por favor tente mais tarde!"
          mmmattosAlerts.showAlert(
            viewController:self as UIViewController,
            style: .alert,
            message: message,
            completion: nil,
            buttons: (.default, "Ok", nil)
          )
          return
        }
        guard let secs = json else {
          
          let message = "Não há Links disponíveis. Por favor tente mais tarde!"
          
          mmmattosAlerts.showAlert(
            viewController:self as UIViewController,
            style: .alert,
            message: message,
            completion: nil,
            buttons: (.default, "Ok", nil)
          )
          return
        }
        completionHandler(secs, nil)
        
    }
  }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
