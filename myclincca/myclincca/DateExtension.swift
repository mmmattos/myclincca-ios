//
//  DateExtension.swift
//  enceenApp10
//
//  Created by Miguel Miranda de Mattos on 25/02/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import Foundation

////Example:
//
//    if let theDate = Date(jsonDate: "/Date(1420420409680)/") {
//    print(theDate)
//} else {
//    print("wrong format")
//}

extension Date {
    init?(jsonDate: Double?) {
//        
//        let prefix = "/Date("
//        let suffix = ")/"
//        
//        // Check for correct format:
//        guard jsonDate.hasPrefix(prefix) && jsonDate.hasSuffix(suffix) else { return nil }
//        
//        // Extract the number as a string:
//        let from = jsonDate.index(jsonDate.startIndex, offsetBy: prefix.characters.count)
//        let to = jsonDate.index(jsonDate.endIndex, offsetBy: -suffix.characters.count)
//        
//        // Convert milliseconds to double
//        guard let milliSeconds = Double(jsonDate[from ..< to]) else { return nil }
//        
//        // Create NSDate with this UNIX timestamp
        guard let ms = jsonDate else { return nil }
        self.init(timeIntervalSince1970: ms/1000.0)
    }
  
  
}
