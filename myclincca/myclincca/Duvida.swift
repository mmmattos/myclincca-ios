//
//  Duvida.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 12/07/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import Foundation

class Duvida: Serializable {
  var pergunta: String!
  var resposta: String!
  var ativo: Bool
  
  init(
    perguntaParam: String,
    respostaParam: String,
    ativoParam: Bool
  ) {
    self.pergunta = perguntaParam
    self.resposta = respostaParam
    self.ativo = ativoParam
  }
}
