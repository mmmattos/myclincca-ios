//
//  myClinDuvidasViewController.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 10/07/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import UIKit

class myClinDuvidasViewController: UIViewController {
  
  
  @IBOutlet weak var duvidasView: UIScrollView!
  
  @IBOutlet weak var backButton: UIButton!
  
  @IBAction func backButtonAction(_ sender: UIButton) {
    
    self.dismiss(animated: true)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    
    duvidasView.isScrollEnabled = true
    
    loadDuvidas() {
      (items,  error) in
      if let duvidas = items {
        Utilities.Log(
          message: "Found Duvidas To Render: \(duvidas)",
          #file,
          Utilities.typeName(some: self),
          #function,
          #line
        )
        self.renderDuvidas(duvidasToRender: duvidas)
      }
    }
    
  }
  
  var fullHeight: Double = 0
  
  func createCustomLabel(text: String, tipo: String, positionY: Double, nbrLines: Int, fontName: String, fontSize: Double, tag: Int ) {
    
    let initialFrame = CGRect(x: CGFloat(0), y: CGFloat(positionY), width: duvidasView.frame.width , height: CGFloat(44))
    let contentInsets = UIEdgeInsets(top: 20, left: 20, bottom: 10, right: 10)
    let paddedFrame = UIEdgeInsetsInsetRect(initialFrame, contentInsets)
    let codedLabel:UILabel = UILabel(frame: paddedFrame)
    codedLabel.numberOfLines=nbrLines
    codedLabel.contentMode = .center
    switch tipo.lowercased() {
    case "pergunta" :
      codedLabel.textAlignment = .left
      codedLabel.textColor=UIColor.darkGray
      codedLabel.backgroundColor = UIColor.clear
      codedLabel.numberOfLines=text.characters.count/20
    case "mensagem" :
      codedLabel.textAlignment = .center
      codedLabel.textColor=UIColor.darkGray
      codedLabel.backgroundColor = UIColor.clear
    case "resposta" :
      codedLabel.textAlignment = .justified
      codedLabel.textColor = UIColor.gray
      codedLabel.backgroundColor = UIColor.clear
      codedLabel.numberOfLines=text.characters.count/20
    default :
      codedLabel.textAlignment = .center
    }
    codedLabel.text = text
    codedLabel.tag = tag
    codedLabel.font=UIFont.init(name: fontName, size: CGFloat(fontSize))
    codedLabel.lineBreakMode = .byWordWrapping;
    codedLabel.sizeToFit()
    self.duvidasView.addSubview(codedLabel)
    
    print ("Rendered Label: \(String(describing: codedLabel.text))")
    
    fullHeight = fullHeight + Double(codedLabel.frame.height)
    
    
  }
  
  
  func renderDuvidas( duvidasToRender: [Duvida]) {
    
    var inativos = 0
    for x:Int in 0..<duvidasToRender.count {      
      let duvida = duvidasToRender[x]
      
      if !duvida.ativo {
        inativos += 1
        continue
      }

      if duvida.pergunta != nil {
        createCustomLabel(
          text: duvida.pergunta!,
          tipo: "pergunta",
          positionY: Double(self.fullHeight),
          nbrLines: 2,
          fontName: "HelveticaNeue-CondensedBlack",
          fontSize: 20,
          tag: x
        )
        self.fullHeight = self.fullHeight + 10
      }
      
      if duvida.resposta != nil {
        createCustomLabel(
          text: duvida.resposta!,
          tipo: "resposta",
          positionY: Double(self.fullHeight),
          nbrLines: 3,
          fontName: "HelveticaNeue-CondensedBold",
          fontSize: 20,
          tag: x
        )
        self.fullHeight = self.fullHeight + 40
      }
    }
    if duvidasToRender.count == 0 || duvidasToRender.count == inativos  {
      createCustomLabel(
        text: "Não há Dúvidas disponíveis para Consulta. Tente mais tarde.",
        tipo: "mansagem",
        positionY: Double(self.fullHeight),
        nbrLines: 5,
        fontName: "HelveticaNeue-CondensedBold",
        fontSize: 20,
        tag: 0
      )
      return
    }
    print ("Full Height is: \(self.fullHeight)")
    let screenSizeWidth = UIScreen.main.bounds.size.width
    duvidasView.contentSize = CGSize(width: screenSizeWidth, height: CGFloat(self.fullHeight+250))
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  
  func loadDuvidas(
    completionHandler: @escaping ([Duvida]?, NSMutableDictionary?) -> ()
    ) -> () {
    
    // MARK: - Read Dúvida
    ApiController.doGetDuvidas()
      {
        (json, error) in
        if let err = error {
          
          Utilities.Log(
            message: "Error reading Duvidas! \(err)",
            #file,
            Utilities.typeName(some: self),
            #function,
            #line
          )
          
          let message = "Erro na leitura das Dúvidas. Por favor tente mais tarde!"
          mmmattosAlerts.showAlert(
            viewController:self as UIViewController,
            style: .alert,
            message: message,
            completion: nil,
            buttons: (.default, "Ok", nil)
          )
          return
        }
        guard let duvs = json else {
          
          let message = "Não há Dúvidas disponíveis. Por favor tente mais tarde!"
          
          mmmattosAlerts.showAlert(
            viewController:self as UIViewController,
            style: .alert,
            message: message,
            completion: nil,
            buttons: (.default, "Ok", nil)
          )
          return
        }
        completionHandler(duvs, nil)
        
    }
  }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
