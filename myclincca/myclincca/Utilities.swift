//
//  Utilities.swift
//  MMattosLtd.
//
//  Created by Miguel Miranda de Mattos on 8/7/16.
//  Copyright © 2016 Miguel Miranda de Mattos. All rights reserved.
//
import UIKit
import Foundation


class Utilities {
  
  /*
   print(typeName(Class1))   // Class1
   print(typeName(Class1())) // Class1
   print(typeName(Class1().f)) // Int -> Int
   
   print(typeName(Struct1)) // Struct1
   print(typeName(Struct1(x: 1))) // Struct1
   print(typeName(Enum1)) // Enum1
   print(typeName(Enum1.X)) // Enum1
   */
  class func typeName(some: Any) -> String {
    return (some is Any.Type) ? "\(some)" : "\(type(of: some))"
  }
  
  class func Log(
    message: String = "", _
    theClassName: String, _
    path: String, _
    function: String, _
    line: Int) {
    let theFunc = String(function.characters.split(separator:"(").map(String.init).first!);
    NSLog("\(path):\(theFunc):(\(String(line))): \(message)")
  }
  
  class func createErrorReportData(
    code: Int,
    message: String,
    error: NSError? = nil) -> NSMutableDictionary?
  {
    let errorReportData: NSMutableDictionary = NSMutableDictionary()
    errorReportData.setValue(code, forKey: "statusCode")
    errorReportData.setValue(message, forKey:"message")
    errorReportData.setValue(error, forKey:"error")
    return errorReportData
  }
  
  
  class func resizeImage(image:UIImage) -> UIImage
  {
    
    let maxWidth = Float(UIScreen.main.bounds.size.width)
    let maxHeight = Float(UIScreen.main.bounds.size.height)
    
    //let maxHeight:Float = 180.0 //your choose height
    //let maxWidth:Float = 180.0  //your choose width
    
    var actualHeight:Float = Float(image.size.height)
    var actualWidth:Float = Float(image.size.width)
    
    var imgRatio:Float = actualWidth/actualHeight
    let maxRatio:Float = maxWidth/maxHeight
    
    if (actualHeight > maxHeight) || (actualWidth > maxWidth)
    {
      if(imgRatio < maxRatio)
      {
        imgRatio = maxHeight / actualHeight;
        actualWidth = imgRatio * actualWidth;
        actualHeight = maxHeight;
      }
      else if(imgRatio > maxRatio)
      {
        imgRatio = maxWidth / actualWidth;
        actualHeight = imgRatio * actualHeight;
        actualWidth = maxWidth;
      }
      else
      {
        actualHeight = maxHeight;
        actualWidth = maxWidth;
      }
    }
    
    let rect:CGRect = CGRect(
      x: 0.0,
      y: 0.0,
      width: CGFloat(actualWidth) ,
      height: CGFloat(actualHeight)
    )
    UIGraphicsBeginImageContext(rect.size)
    image.draw(in: rect)
    
    let img:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    let imageData:NSData = UIImageJPEGRepresentation(img, 1.0)! as NSData
    UIGraphicsEndImageContext()
    
    return UIImage(data: imageData as Data)!
  }

  
}
