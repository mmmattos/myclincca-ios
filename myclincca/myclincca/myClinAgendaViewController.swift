//
//  myClinAgendaViewController.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 16/04/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import UIKit
import JBDatePicker
import KeychainAccess

class myClinAgendaViewController: UIViewController,
  JBDatePickerViewDelegate,
  UICollectionViewDataSource,
  UICollectionViewDelegateFlowLayout,
  UICollectionViewDelegate
{
  
  var selectedDate: String = ""
  var selectedTime: String = ""
  var cleanCollectionView: Bool = false
  var horariosAgenda: [HorarioAgenda] = []
  
  
  @IBOutlet weak var backButton: UIButton!
  
  @IBAction func backButtonAction(_ sender: UIButton) {
    self.dismiss(animated: true)
  }
  
  @IBOutlet weak var monthLabel: UILabel!
  
  
  
  @IBOutlet weak var jbDatePicker: JBDatePickerView!
  
  
  @IBOutlet weak var nextMonth: UIButton!
  
  
  @IBAction func nextMonthAction(_ sender: Any) {
    jbDatePicker.loadNextView()
    self.cleanCollectionView = true
    self.horariosAgenda = []
    self.selectedDate = ""
    self.selectedTime = ""
    self.refreshUI()
    self.resetHorariosCollection()
  }
  
  @IBOutlet weak var previousMonth: UIButton!
  
  @IBAction func previousMonthAction(_ sender: Any) {
    jbDatePicker.loadPreviousView()
    self.cleanCollectionView = true
    self.horariosAgenda = []
    self.selectedDate = ""
    self.selectedTime = ""
    self.refreshUI()
    self.resetHorariosCollection()
  }
  
  fileprivate let sectionInsets = UIEdgeInsets(
    top: 0.0,
    left: 0.5,
    bottom: 0.0,
    right: 0.0
  )
  
  
  let horarios = Horario(
    startHour: 7.5,
    intervalMinutes: 30,
    endHour: 19
  )
  
  //print(horarios.timeRepresentations)
  
  @IBOutlet var collectionView: UICollectionView!
  
  
  override func viewDidAppear(_ animated: Bool) {
 
  }
  
  override func viewWillAppear(_ animated: Bool) {
    self.loadSelectedDate(selectedDate: self.selectedDate)
  }
  
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    
    jbDatePicker.delegate = self
    jbDatePicker.presentedMonthView.sizeToFit()
    
    // Get current date
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    self.selectedDate = formatter.string(from: Date())
    
    
    collectionView.delegate = self
    collectionView.dataSource = self
    
    let cellWidth : CGFloat = collectionView.frame.size.width / 3.4
    let cellheight : CGFloat = collectionView.frame.size.height / 8.0
    let cellSize = CGSize(width: cellWidth , height:cellheight)
    
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .vertical //.horizontal
    layout.estimatedItemSize = CGSize(width: 89, height: 30)
    layout.itemSize = cellSize
    layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    layout.minimumLineSpacing = 5.0
    layout.minimumInteritemSpacing = 1.0
    collectionView.setCollectionViewLayout(layout, animated: true)
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  func resetHorariosCollection() {
    for item:myClinAgendaCollectionViewCell in collectionView.visibleCells as! [myClinAgendaCollectionViewCell] {
      item.backgroundColor = UIColor.white
      item.horarioLabel.textColor = UIColor.white
    }
    self.refreshUI()
  }
  
  
  func getCurrentTimeslots(forDate dataP:String) {
    
    // MARK: - Get the Agenda for the Day
    ApiController.getAgenda(dataP: dataP)
    {
      (horariosAgenda, error) in
      if (horariosAgenda != nil) {
        self.horariosAgenda = horariosAgenda!
        self.refreshUI()
      }
      if (error != nil) {
        print ("ERROR!!! \(String(describing: error))")
        var message = ""
        
        //          mmmattosAlerts.showAlert(
        //            viewController:self as UIViewController,
        //            style: .alert,
        //            message: finalMessage,
        //            completion: nil,
        //            buttons: (.default, "Ok", nil)
        //          )
        return;
      }
    }
  }
  
  
  func refreshUI() {
    DispatchQueue.main.async {
      self.collectionView.reloadData()
    }
  }
  
  
  // MARK: - JBDatePickerViewDelegate implementation
  
  
  
  func didSelectDay(_ dayView: JBDatePickerDayView) {
    
    if let d1 = dayView.date {
      var dataP: String = ""
      let d = d1.description
      let index = d.index(d.startIndex, offsetBy: 10)
      dataP = d.substring(to: index)  // YYYY-MM-DD
      self.loadSelectedDate(selectedDate: dataP)
    }
  }
  
  
  
  func shouldAllowSelectionOfDay(_ date: Date?) -> Bool {
    guard let date = date else {return true}
    let weekday = Calendar.current.component(.weekday, from: date)
    if weekday == 1 || weekday == 7 {
      return false
    }
    return true
  }
  
  
  
  //  var dateToShow: Date {
  //    if let date = dateToSelect {
  //      return date
  //    }
  //    else{
  //      return Date()
  //    }
  //  }
  
  
  var weekDaysViewHeightRatio: CGFloat {
    return 0.2
  }
  
  var fontForDayLabel: JBFont {
    return JBFont(name: "HelveticaNeue-CondensedBold", size: .large)
  }
  
  var colorForDayLabelInMonth: UIColor {
    return .black
  }
  
  func loadSelectedDate ( selectedDate: String) {
    print("DataP: \(selectedDate)")
    self.selectedDate = selectedDate
    //self.resetHorariosCollection()
    self.getCurrentTimeslots(forDate: selectedDate)
  }
  
  
  func didPresentOtherMonth(_ monthView: JBDatePickerMonthView) {
    self.monthLabel.text = jbDatePicker.presentedMonthView.monthDescription
  }
  
  
  // MARK: - UICollectionView implementation
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  
  
  func collectionView(_
    collectionView: UICollectionView,
                      numberOfItemsInSection section: Int) -> Int {
    if self.cleanCollectionView {
      self.cleanCollectionView = false
      return 0
    }
    return 20; //horariosAgenda.count //horarios.timeRepresentations.count
  }
  
  func collectionView(_
    collectionView: UICollectionView,
                      cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! myClinAgendaCollectionViewCell
    cell.layer.borderWidth = 1
    cell.layer.cornerRadius = 8
    let keychain = Keychain(service: "com.myclincca.myclincca-token")
    let myId = try! keychain.get(Constants.KeychainKeys.SessionUserId)
    
    if horariosAgenda.count > 0 {
      let cellData = horariosAgenda[indexPath.item]
      //horarios.timeRepresentations[indexPath.item]
      cell.horarioLabel.text = cellData.horario
      if cellData.user == myId && !cellData.disponivel!{
        cell.backgroundColor = UIColor.green
        cell.horarioLabel.textColor = UIColor.black
      } else {
        if cellData.disponivel! {
          cell.backgroundColor = UIColor.white
          cell.horarioLabel.textColor = UIColor.black
        } else {
          cell.backgroundColor = UIColor.red
          cell.horarioLabel.textColor = UIColor.white
        }
      }
    }
    
    return cell
  }
  
  func collectionView(_
    collectionView: UICollectionView,
                      didSelectItemAt indexPath: IndexPath) {
    let keychain = Keychain(service: "com.myclincca.myclincca-token")
    let myId = try! keychain.get(Constants.KeychainKeys.SessionUserId)
    let cell: myClinAgendaCollectionViewCell = collectionView.cellForItem(at: indexPath) as! myClinAgendaCollectionViewCell
    if !self.selectedDate.isEmpty {
      let cellData = horariosAgenda[indexPath.item]
      // print("CELLDATA CLICKED: \(cellData.toJsonString(prettyPrinted: true))")
      if cellData.disponivel! || (!cellData.disponivel! && myId == cellData.user! ) {
        //Perform segue to map.
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc : myClinAgendaConfirmViewController = mainStoryboard.instantiateViewController(withIdentifier: "AgendaConfirmVC") as! myClinAgendaConfirmViewController
        let dataEHora = "\(self.selectedDate) - \(String(describing: cell.horarioLabel.text!))"
        vc.dataEhoraStr = dataEHora
        vc.agendamentoId = horariosAgenda[indexPath.item].id!

        vc.isEditMode = false
        if (cellData.user == myId) {
          vc.isEditMode = true
          vc.pacienteNomeEdit = cellData.pacienteNome!
          vc.pacienteTelefoneEdit = cellData.pacienteTelefone!
          vc.pacienteEmailEdit = cellData.pacienteEmail!
          vc.pacienteNascEdit = cellData.pacienteNasc!
          vc.procedimentoMedicoHonorariosEdit = cellData.procedimentoMedHonorarios!
          vc.procedimentoAnatomoObsEdit = cellData.procedimentoAnatomoObs!
          vc.procedimentoIdEdit = cellData.procedimento!
        }
        self.present(vc, animated: true, completion: {
          self.getCurrentTimeslots(forDate: self.selectedDate)
        })
      }
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
    collectionView.scrollToItem(at:IndexPath(item: indexPath.item, section: 0), at: .centeredHorizontally, animated: false)
    
    return true
  }
  
  func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
    return true
  }
  
  
  func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
  }
}
