//
//  RegisterStep1ViewController.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 04/05/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import Foundation
import UIKit
import KeychainAccess


class RegisterStep1ViewController:
  UIViewController
  , UITextFieldDelegate
  , UITextViewDelegate
, UIScrollViewDelegate {
  
  private let SCREEN_SIZE = UIScreen.main.bounds
  private let REGISTER_BOX_HEIGHT_FACTOR:CGFloat = 667.0/230.0
  private let REGISTER_BOX_MIN_HEIGHT:CGFloat = 320.0
  
  var userNome: String = ""
  var userEmail: String = ""
  var userPassword: String = ""
  var userEspecialidade: String = ""
  var userCremers: String = ""
  var userTelefone: String = ""
  var userRg: String = ""
  var userCpf: String = ""
  var userEstadoCivil: String = ""
  
  @IBOutlet weak var registerStep1Box: UIView!
  @IBOutlet weak var logoMobile: UIImageView!
  @IBOutlet weak var backButton: UIButton!
  @IBOutlet weak var telefoneTextBox: UITextField!
  @IBOutlet weak var rgTextBox: UITextField!
  @IBOutlet weak var cpfTextBox: UITextField!
  @IBOutlet weak var antButton: UIButton!
  @IBOutlet weak var proxButton: UIButton!
  @IBOutlet weak var estadoCivilTextBox: UITextField!
  
  
  
  @IBAction func backButtonAction(_ sender: Any) {
    //self.dismiss(animated: true)
    let tempRegStore = UserDefaults.standard
    
    // Clean up TEMP data
    tempRegStore.removeObject(forKey: "register.nome")
    tempRegStore.removeObject(forKey: "register.email")
    tempRegStore.removeObject(forKey: "register.pw")
    tempRegStore.removeObject(forKey: "register.especialidade")
    tempRegStore.removeObject(forKey: "register.cremers")
    tempRegStore.removeObject(forKey: "register.cpf")
    tempRegStore.removeObject(forKey: "register.rg")
    tempRegStore.removeObject(forKey: "register.telefone")
    tempRegStore.removeObject(forKey: "register.estado-civil")
    tempRegStore.removeObject(forKey: "register.endereco")
    tempRegStore.removeObject(forKey: "register.cidade")
    tempRegStore.removeObject(forKey: "register.estado")
    
    //// Perform segue to RegisterStep1VC.
    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    let vc : myClinMenuViewController = mainStoryboard.instantiateViewController(
      withIdentifier: "myclinccaMenu") as! myClinMenuViewController
    
    self.present(vc, animated: true, completion: nil)
  }
  
  @IBAction func antButtonAction(_ sender: Any) {
    //Perform segue to RegisterStep1VC.
    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    let vc :RegisterViewController = mainStoryboard.instantiateViewController(
      withIdentifier: "RegisterVC") as! RegisterViewController
    
    // Need to refresh typed data in prev step?
    let userCpf = (cpfTextBox.text ?? "")
    let userRg = (rgTextBox.text ?? "")
    let userTelefone = (telefoneTextBox.text ?? "")
    let userEstadoCivil = (estadoCivilTextBox.text ?? "")
    
    let tempRegStore = UserDefaults.standard
    tempRegStore.set(userCpf, forKey: "register.cpf")
    tempRegStore.set(userRg, forKey: "register.rg")
    tempRegStore.set(userTelefone, forKey: "register.telefone")
    tempRegStore.set(userEstadoCivil, forKey: "register.estado-civil")
    
    self.present(vc, animated: true, completion: nil)
  }
  
  @IBAction func proxButtonAction(_ sender: Any) {
    // VALIDATE
    let userCpf = (cpfTextBox.text ?? "")
    let userRg = (rgTextBox.text ?? "")
    let userTelefone = (telefoneTextBox.text ?? "")
    let userEstadoCivil = (estadoCivilTextBox.text ?? "")
    
    //Validate fields
    if (userCpf.isEmpty || userRg.isEmpty ||
      userTelefone.isEmpty || userEstadoCivil.isEmpty)
    {
      mmmattosAlerts.showAlert(
        viewController: self as UIViewController,
        style: .alert,
        message: "Todos os campos são obrigatórios!",
        completion: nil,
        buttons: (.default, "Ok", nil)
      )
      return;
    }
    
    let tempRegStore = UserDefaults.standard
    tempRegStore.setValue(userCpf, forKey: "register.cpf")
    tempRegStore.setValue(userRg, forKey: "register.rg")
    tempRegStore.setValue(userTelefone, forKey: "register.telefone")
    tempRegStore.setValue(userEstadoCivil, forKey: "register.estado-civil")
    
    //Perform segue to RegisterStep2VC.
    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    let vc : RegisterStep2ViewController = mainStoryboard.instantiateViewController(
      withIdentifier: "RegisterStep2VC") as! RegisterStep2ViewController
    
    
    self.present(vc, animated: true, completion: nil)
  }
  
  // MARK: - Close Keyboard on Return key.
  func textFieldShouldReturn(_ userText: UITextField) -> Bool {
    userText.resignFirstResponder()
    return true
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    registerStep1Box.layer.cornerRadius = 16
    registerStep1Box.layer.masksToBounds = true  // optional
    var desiredHeight :CGFloat = SCREEN_SIZE.height / REGISTER_BOX_HEIGHT_FACTOR
    desiredHeight = (desiredHeight < REGISTER_BOX_MIN_HEIGHT ? REGISTER_BOX_MIN_HEIGHT : desiredHeight)
    let boxY = logoMobile.frame.size.height + 30.0
    registerStep1Box.frame.origin.y = boxY
    registerStep1Box.frame.size.height = desiredHeight
    
    self.cpfTextBox.delegate = self
    self.rgTextBox.delegate = self
    self.telefoneTextBox.delegate = self
    self.estadoCivilTextBox.delegate = self
  }
  
  
  override func viewDidAppear(_ animated: Bool) {
  
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    let tempRegStore = UserDefaults.standard
    
    // TEMP data 2nd screen
    if let cpf = tempRegStore.string(forKey: "register.cpf") {
      self.cpfTextBox.text = cpf
    }
    if let rg = tempRegStore.string(forKey: "register.rg") {
      self.rgTextBox.text = rg
    }
    if let telefone = tempRegStore.string(forKey: "register.telefone") {
      self.telefoneTextBox.text = telefone
    }
    if let estadoCivil = tempRegStore.string(forKey: "register.estado-civil") {
      self.estadoCivilTextBox.text = estadoCivil
    }
  }
}
