//
//  UIColorExtension.swift
//  enceenApp10
//
//  Created by Miguel Miranda de Mattos on 27/02/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(hex: Int, alpha: Double = 1.0) {
        self.init(red: CGFloat((hex>>16)&0xFF)/255.0, green: CGFloat((hex>>8)&0xFF)/255.0, blue: CGFloat((hex)&0xFF)/255.0, alpha: CGFloat(255 * alpha) / 255)
    }
}

/*
 
 USAGE:
 
 UIColor(hex: 0xffffff) // r 1.0 g 1.0 b 1.0 a 1.0
 UIColor(hex: 0xffffff, alpha: 0.5) // r 1.0 g 1.0 b 1.0 a 0.5
 
 */
