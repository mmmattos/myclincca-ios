//
//  myClinClinicaViewController.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 04/03/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import UIKit
import KeychainAccess
import Alamofire
import AlamofireImage


class myClinClinicaViewController: UIViewController {
  
    @IBOutlet weak var clinicaView: UIScrollView!
    
  var token: String = ""
  var sections = [ClinicaSection]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    
    clinicaView.isScrollEnabled = true

    loadClinicaSections() {
      (sections,  error) in
      if let clinicaSections = sections {
        Utilities.Log(
          message: "Found Sections To Render: \(clinicaSections)",
          #file,
          Utilities.typeName(some: self),
          #function,
          #line
        )
        self.renderLoadedSections(sectionsToRender: clinicaSections)
      }
    }
  }
  
  @IBOutlet weak var backButton: UIButton!
  
  
  @IBAction func backButtonAction(_ sender: UIButton) {
    
    self.dismiss(animated: true)
  }
  
//  func scrollViewDidScroll(scrollView: UIScrollView) {
//    if scrollView.contentOffset.x != 0 {
//      scrollView.contentOffset.x = 0
//    }
//  }
  
  
  var fullHeight: Double = 20
  
  func createCustomLabel(text: String, tipo: String, positionY: Double, nbrLines: Int, fontName: String, fontSize: Double ) {
    let codedLabel:UILabel = UILabel()
    let newPositionY = fullHeight + 20  // use positionY otherwise...
    codedLabel.frame = CGRect(x: CGFloat(10), y: CGFloat(newPositionY), width: clinicaView.frame.width - 20 , height: CGFloat(30))
    switch tipo.lowercased() {
      case "texto" :
        codedLabel.textAlignment = .justified
      case "lista" :
        codedLabel.textAlignment = .left
      default :
        codedLabel.textAlignment = .center
      
    }
    
    codedLabel.numberOfLines=nbrLines
    let tok =  text.components(separatedBy: "\n")
    let newNbrLines = tok.count
    if newNbrLines > nbrLines {
      codedLabel.numberOfLines = newNbrLines
    }
    
    codedLabel.text = text
    codedLabel.textColor=UIColor.darkGray
    codedLabel.font=UIFont.init(name: fontName, size: CGFloat(fontSize))
    codedLabel.backgroundColor=UIColor.clear
    codedLabel.sizeToFit()
    self.clinicaView.addSubview(codedLabel)
    
    //// Play with AutoConstraints...
    // codedLabel.translatesAutoresizingMaskIntoConstraints = false
    // codedLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
    // codedLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
    // codedLabel.centerXAnchor.constraint(equalTo: codedLabel.superview!.centerXAnchor).isActive = true
    // codedLabel.centerYAnchor.constraint(equalTo: codedLabel.superview!.centerYAnchor).isActive = true
    
    //print ("Rendered Label: \(String(describing: codedLabel.text))")
    
    fullHeight = fullHeight + Double(codedLabel.frame.height)
    
    
  }
  
  
  func renderLoadedSections( sectionsToRender: [ClinicaSection]) {
    let sectionsSorted = sectionsToRender.sorted { $0.ordem < $1.ordem }
    var sectionY = 20
    for section in sectionsSorted {
      Utilities.Log(
        message: "Found Section To Render: \(section)",
        #file,
        Utilities.typeName(some: self),
        #function,
        #line
      )
      
      if section.secao != nil {
        createCustomLabel(
          text: section.secao!,
          tipo: section.tipo,
          positionY: Double(fullHeight),
          nbrLines: 2,
          fontName: "HelveticaNeue-CondensedBlack",
          fontSize: 18
        )
        fullHeight = fullHeight + 20
        //sectionY = sectionY + 40
      }
      
      if section.conteudo != nil {
        if let conteudo = section.conteudo {
          var nbrLines = 0
          switch section.tipo.lowercased() {
          case "texto" :
//            if conteudoArray.count > 1 {
//              nbrLines = conteudoArray.count
//              for i in 0..<conteudoArray.count {
//                createCustomLabel(
//                  text: conteudoArray[i],
//                  tipo: section.tipo,
//                  positionY: Double(sectionY),
//                  nbrLines: nbrLines,
//                  fontName: "HelveticaNeue-CondensedBold",
//                  fontSize: 16)
//              }
//            } else {
              nbrLines = conteudo.characters.count / 25
              createCustomLabel(
                text: conteudo,
                tipo: section.tipo,
                positionY: fullHeight,
                nbrLines: nbrLines,
                fontName: "HelveticaNeue-CondensedBold",
                fontSize: 16)

//            }
            fullHeight = fullHeight + 20
//          case "lista" :
//            nbrLines = conteudoArray.count
//            var bulletedList: String = ""
//            for i in 0..<nbrLines {
//              let bulletPoint: String = "\u{2022}"
//              bulletedList += "\(bulletPoint) \(conteudoArray[i])\n"
//            }
//            createCustomLabel(
//              text: bulletedList,
//              tipo: section.tipo,
//              positionY: Double(sectionY),
//              nbrLines: nbrLines,
//              fontName: "HelveticaNeue-CondensedBold",
//              fontSize: 16 )
//            sectionY = sectionY + 60
          case "imagem" :
            let screenSizeWidth = UIScreen.main.bounds.size.width
            let imageName = conteudo
            Utilities.Log(
              message: "Found Image URI: \(imageName)",
              #file,
              Utilities.typeName(some: self),
              #function,
              #line
            )
            var imageView : UIImageView
            imageView  = UIImageView(frame:CGRect(
              x: CGFloat(10),
              y: CGFloat(fullHeight + 20),
              width: screenSizeWidth-20,
              height: 240));
//            let imageUrl = "\(Constants.AppUrls.BackendUrl)/\(imageName)"
//            print("Image URL: \(imageUrl)")
//            let downloadUrl = NSURL(string: imageUrl)!
//            imageView.af_setImage(withURL: downloadUrl as URL)
            imageView.image = UIImage(named: "mapa")
            imageView.contentMode = .scaleAspectFit
                self.clinicaView.addSubview(imageView)
            fullHeight = fullHeight + 240
            //sectionY = sectionY + 300
          default :
            Utilities.Log(
              message: "Could not detect type of section: \(section.tipo)",
              #file,
              Utilities.typeName(some: self),
              #function,
              #line
            )
          }
          //fullHeight = fullHeight + Double(10 * nbrLines)
          //sectionY = sectionY + (10 * nbrLines)
        }
      }
    }
    print ("Full Height is: \(self.fullHeight)")
    let screenSizeWidth = UIScreen.main.bounds.size.width
    clinicaView.contentSize = CGSize(width: screenSizeWidth, height: CGFloat(self.fullHeight+950))
  }
  

  
  func loadClinicaSections(
    completionHandler: @escaping ([ClinicaSection]?, NSMutableDictionary?) -> ()
    ) -> () {
    
    // MARK: - Read the Clinica Sections
    ApiController.doGetSecoesClinica()
      {
        (json, error) in
        if let err = error {
          
          Utilities.Log(
            message: "Error reading ClinicaSections! \(err)",
            #file,
            Utilities.typeName(some: self),
            #function,
            #line
          )
          
          let message = "Erro na leitura das seções da Clínica. Por favor tente mais tarde!"
          mmmattosAlerts.showAlert(
            viewController:self as UIViewController,
            style: .alert,
            message: message,
            completion: nil,
            buttons: (.default, "Ok", nil)
          )
          return
        }
        guard let secs = json else {
          
          let message = "Não há informações sobre a Clínica. Por favor tente mais tarde!"
          
          mmmattosAlerts.showAlert(
            viewController:self as UIViewController,
            style: .alert,
            message: message,
            completion: nil,
            buttons: (.default, "Ok", nil)
          )
          return
        }
        completionHandler(secs, nil)

    }
  }
  
  func translateClinicaErrorCode(status :Int, message: String) -> String {
    
    var finalMessage = "Clínica - Erro. "
    
    switch status {
    case 404:
      finalMessage = finalMessage + "Recurso não localizado! Entre em contato com o adm. do sistema"
      break;
    case 401:
      finalMessage = finalMessage + "Acesso não autorizado. Por favor tente novamente mais tarde ou verifique com o adm. do sistema."
      break;
    case 400:
      finalMessage = finalMessage + "Por favor tente novamente mais tarde."
      break;
    case 500:
      finalMessage = finalMessage + "Un Erro não tratado foi detectado. Por favor tente novamente mais tarde."
      break;
    default:
      finalMessage = finalMessage + "Por favor tente novamente mais tarde."
      break;
      
    }
    
    return finalMessage
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}
