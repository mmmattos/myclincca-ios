//
//  TipoProcedimento.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 13/07/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import Foundation

class TipoProcedimento: Serializable {
  var tipo: String!
  var descricao: String!
  var emailAutor: String!
  var ativo: Bool
  
  init(
    tipoParam: String,
    descricaoParam: String,
    emailAutorParam: String,
    ativoParam: Bool
    ) {
      self.tipo = tipoParam
      self.descricao = descricaoParam
      self.emailAutor = emailAutorParam
      self.ativo = ativoParam
  }
}
