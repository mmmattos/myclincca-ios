//
//  myClinAgendaConfirmViewController.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 05/05/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import UIKit
import KeychainAccess

class myClinAgendaConfirmViewController: UIViewController
  , UITextViewDelegate
  , UITextFieldDelegate
  , UIPickerViewDelegate
  , UIPickerViewDataSource
  , UIScrollViewDelegate {
  
  var activeTextField = UITextField()
  let placeholderNomePac = "Digite o Nome Completo"
  let placeholderTelPac = "Digite o Telefone"
  let placeholderEmailPac = "Digite o endereço de E-Mail"
  let placeholderNascPac = "Digite Data Nascimento DD/MM/AAAA"
  let placeholderProcHonor = "Digite seus Honorários (R$)"
  let placeholderProcObs = "Escreva suas Observações"


  
  @IBOutlet weak var topLogo: UIImageView!
  @IBOutlet weak var dataEhora: UILabel!
  @IBOutlet weak var pacienteNomeLabel: UILabel!
  @IBOutlet weak var pacienteNomeTextBox: UITextField!
  
    @IBOutlet weak var pacienteNascTextBox: UITextField!
    @IBOutlet weak var pacienteEmailTextBox: UITextField!

  @IBOutlet weak var pacienteTelefoneTextBox: UITextField!
  @IBOutlet weak var procedimentoLabel: UILabel!
  @IBOutlet weak var procedimentoPicker: UIPickerView!
  
  @IBOutlet weak var procedimentoObs: UITextView!
  @IBOutlet weak var confirmAgenda: UIButton!
  @IBOutlet weak var backButton: UIButton!
 
    @IBOutlet weak var procedimentoHonorarios: UITextField!
    
    @IBOutlet weak var procedimentoAnatomoObs: UITextField!
    
    @IBOutlet var scrollView: UIScrollView!
    
  var dataEhoraStr: String = ""
  var agendamentoId: String = ""
  var isEditMode: Bool = false
  var selectedRow: Int = 0
  var pacienteNomeEdit: String = ""
  var pacienteTelefoneEdit: String = ""
  var pacienteEmailEdit: String = ""
  var pacienteNascEdit: String = ""
  var procedimentoIdEdit: String = ""
  var procedimentoEdit: Procedimento!
  var medicoIdEdit: String = ""
  var detalhesEdit: String = ""
  var procedimentoMedicoHonorariosEdit: String = ""
  var procedimentoAnatomoObsEdit: String = ""
  var procedimentos: [Procedimento] = []
  
  @IBAction func confirmAgendaAction(_ sender: Any) {
    
    if isEditMode {
      cancelarAgendamento()
      return
    }
    
    if (self.pacienteNomeTextBox.text!.isEmpty) || self.pacienteTelefoneTextBox.text!.isEmpty {
      return
    }
    confirmarAgendamento()
  }
  
  
  func cancelarAgendamento() {

    let reqdSlots: Double = self.procedimentoEdit.duracao/30
    
    
    ApiController.cancelaAgendamento(
      agendamentoId: self.agendamentoId,
      reqdSlots: reqdSlots)
    {
      (confirmacao, error) in
      if (confirmacao != nil) {
        // TODO
        let conf = confirmacao!
        Utilities.Log (
          message:"CONFIRMACAO: \(String(describing: conf)) ",
          #file,
          Utilities.typeName(some: self),
          #function,
          #line
        )
        mmmattosAlerts.showAlert(
          viewController:self as UIViewController,
          style: .alert,
          message: conf,
          completion: { self.dismiss(animated: true) },
          buttons: (.default, "Ok", nil)
        )
        // self.refreshUI()
        // self.dismiss(animated: true)
        return
      }
      if (error != nil) {
        Utilities.Log (
          message:"ERRO DURANTE A CONFIRMACAO: \(String(describing: error)) ",
          #file,
          Utilities.typeName(some: self),
          #function,
          #line
        )
        let message = error!["message"]!
        mmmattosAlerts.showAlert(
          viewController:self as UIViewController,
          style: .alert,
          message: message as? String,
          completion: nil,
          buttons: (.default, "Ok", nil)
        )
        return;
      }
    }
  }
  
  func confirmarAgendamento() {
  
    let keychain = Keychain(service: "com.myclincca.myclincca-token")
    
    let procedimento = procedimentos[self.procedimentoPicker.selectedRow(inComponent: 0)]
    let procedimentoId = procedimento.id
    let duracao = procedimento.duracao
    let reqdSlots = duracao/30
    let medicoId = try! keychain.get(Constants.KeychainKeys.SessionUserId)
    
    let pN = pacienteNomeTextBox.text ?? ""
    let pE = pacienteEmailTextBox.text ?? ""
    let pT = pacienteTelefoneTextBox.text ?? ""
    let pD = pacienteNascTextBox.text ?? ""
    
    if pN.isEmpty || pN == placeholderNomePac ||
       pT.isEmpty || pT == placeholderTelPac ||
       //pE.isEmpty || pE == placeholderEmailPac ||
       pD.isEmpty || pD == placeholderNascPac {
      
       mmmattosAlerts.showAlert(
        viewController:self as UIViewController,
        style: .alert,
        message: "Todos os campos devem ser preenchidos para Confirmar o Agendamento!",
        completion: nil,
        buttons: (.default, "Ok", nil)
      
      )
      return
    }
    
    
    ApiController.confirmaAgendamento(
      agendamentoId: self.agendamentoId,
      medicoId: medicoId!,
      procedimentoId: procedimentoId!,
      pacienteNome: self.pacienteNomeTextBox.text!,
      pacienteTelefone: self.pacienteTelefoneTextBox.text!,
      pacienteEmail: self.pacienteEmailTextBox.text!,
      pacienteNasc: self.pacienteNascTextBox.text!,
      medicoHonorarios: self.procedimentoHonorarios.text!,
      medicoAnatomoObs: self.procedimentoAnatomoObs.text!,
      reqdSlots: reqdSlots)
    {
      (confirmacao, error) in
      if (confirmacao != nil) {
        // TODO
        let conf = confirmacao!
        Utilities.Log (
          message:"CONFIRMACAO: \(String(describing: conf)) ",
          #file,
          Utilities.typeName(some: self),
          #function,
          #line
        )
        mmmattosAlerts.showAlert(
          viewController:self as UIViewController,
          style: .alert,
          message: conf,
          completion: { self.dismiss(animated: true) },
          buttons: (.default, "Ok", nil)
        )
        // self.refreshUI()
        // self.dismiss(animated: true)
        return
      }
      if (error != nil) {
        Utilities.Log (
          message:"ERRO DURANTE A CONFIRMACAO: \(String(describing: error)) ",
          #file,
          Utilities.typeName(some: self),
          #function,
          #line
        )
        let message = error!["message"]!
        mmmattosAlerts.showAlert(
          viewController:self as UIViewController,
          style: .alert,
          message: message as? String,
          completion: nil,
          buttons: (.default, "Ok", nil)
        )
        return;
      }
    }
  }
  
  @IBAction func backButtonAction(_ sender: Any) {
    self.dismiss(animated: true)
  }
  
  func getProcedimentoById(id: String) -> Procedimento? {
    for procedimento in procedimentos {
      if procedimento.id == id { return procedimento }
    }
    return nil
  }
  
  func refreshUI() {
    DispatchQueue.main.async {
      self.procedimentoPicker.reloadAllComponents()
    }
  }
  
  func loadProcedimentos(
    completionHandler: @escaping () -> ()) -> ()
  {
    
    // MARK: - Get Procedimentos Ativos
    ApiController.getProcedimentos()
      {
        (procedimentos, error) in
        if (procedimentos != nil) {
          self.procedimentos = procedimentos!
          self.refreshUI()
        }
        if (error != nil) {
          print ("ERROR!!! \(String(describing: error))")
        }
        completionHandler()
    }
  }
  
  
  override func viewWillAppear(_ animated: Bool) {
    
    if isEditMode {
      self.confirmAgenda.backgroundColor = UIColor.red
      self.confirmAgenda.setTitleColor(UIColor.white, for: [.normal, .selected, .selected,.focused])
      self.confirmAgenda.titleLabel?.textAlignment = .center
    }
    self.startKeyboardObserver()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    self.stopKeyboardObserver()
  }
  
  private func startKeyboardObserver(){
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(myClinAgendaConfirmViewController.keyboardWillShow),
      name: NSNotification.Name.UIKeyboardWillShow, object: nil
    )
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(myClinAgendaConfirmViewController.keyboardWillHide),
      name: NSNotification.Name.UIKeyboardWillHide, object: nil
    )
  }
  
  private func stopKeyboardObserver() {
    
    NotificationCenter.default.removeObserver(
      self, name: NSNotification.Name.UIKeyboardWillShow, object: nil
    )
    NotificationCenter.default.removeObserver(
      self, name: NSNotification.Name.UIKeyboardWillHide, object: nil
    )
  }

  func keyboardWillShow(notification: NSNotification) {
    guard let info:[AnyHashable:Any] = notification.userInfo,
      let keyboardSize:CGSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size else { return }
    
    let insets:UIEdgeInsets = UIEdgeInsets(top: self.scrollView.contentInset.top, left: 0.0, bottom: keyboardSize.height, right: 0.0)
    
    self.scrollView.contentInset = insets
    self.scrollView.scrollIndicatorInsets = insets
  }
  
  func keyboardWillHide(notification: NSNotification) {
    //guard let info:[AnyHashable:Any] = notification.userInfo,
    //let keyboardSize:CGSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size else { return }
    
    let insets:UIEdgeInsets = UIEdgeInsets.zero;
    //UIEdgeInsets(top: self.scrollView.contentInset.top, left: 0.0, bottom: keyboardSize.height, right: 0.0)
    
    self.scrollView.contentInset = insets
    self.scrollView.scrollIndicatorInsets = insets
  }
  
  override func viewDidLayoutSubviews() {
    self.scrollView.contentSize = self.view.frame.size
  }
  
  override func viewDidAppear(_ animated: Bool) {

    self.procedimentoPicker.isUserInteractionEnabled = true
    
    if self.isEditMode {
      self.confirmAgenda.titleLabel!.text = "CANCELAR AGENDA"
      self.pacienteNomeTextBox.text = self.pacienteNomeEdit
      self.pacienteNomeTextBox.isEnabled = false
      self.pacienteTelefoneTextBox.text = self.pacienteTelefoneEdit
      self.pacienteTelefoneTextBox.isEnabled = false
      self.pacienteEmailTextBox.text = self.pacienteEmailEdit
      self.pacienteEmailTextBox.isEnabled = false
      self.pacienteNascTextBox.text = self.pacienteNascEdit
      self.pacienteNascTextBox.isEnabled = false
      self.procedimentoHonorarios.text = self.procedimentoMedicoHonorariosEdit
      self.procedimentoHonorarios.isEnabled = false
      self.procedimentoAnatomoObs.text = self.procedimentoAnatomoObsEdit
      self.procedimentoAnatomoObs.isEnabled = false
      self.procedimentoPicker.isUserInteractionEnabled = false
    }
    
    loadProcedimentos()
    {
      () in
      
      if self.isEditMode {
        let procedimento = self.getProcedimentoById(id: self.procedimentoIdEdit)
        if procedimento !=  nil {
          self.selectedRow = self.procedimentos.index(of: procedimento!)!
          self.procedimentoEdit = procedimento
          self.buildDetalhes(item: self.procedimentos[self.selectedRow])
        }
      }
      self.procedimentoPicker.selectRow(self.selectedRow, inComponent: 0, animated: true)
      self.pickerView(self.procedimentoPicker, didSelectRow: self.selectedRow, inComponent: 0)
    }
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    procedimentoPicker.delegate = self
    procedimentoPicker.dataSource = self
    scrollView.delegate = self
    
    self.dataEhora.text = self.dataEhoraStr
    
    topLogo.bounds = view.frame.insetBy(dx: 10.0, dy: 10.0)
    
    pacienteNomeTextBox.text = placeholderNomePac
    pacienteNomeTextBox.textAlignment = NSTextAlignment.center
    pacienteNomeTextBox.textColor = UIColor.gray
    pacienteNomeTextBox.layoutMargins = UIEdgeInsets.init(top: 0.0,left: 0.0,bottom: 0.0,right: 0.0)
    pacienteNomeTextBox.delegate = self

    pacienteEmailTextBox.text = placeholderEmailPac
    pacienteEmailTextBox.textAlignment = NSTextAlignment.center
    pacienteEmailTextBox.textColor = UIColor.gray
    pacienteEmailTextBox.layoutMargins = UIEdgeInsets.init(top: 0.0,left: 0.0,bottom: 0.0,right: 0.0)
    pacienteEmailTextBox.delegate = self
    
    pacienteNascTextBox.text = placeholderNascPac
    pacienteNascTextBox.textAlignment = NSTextAlignment.center
    pacienteNascTextBox.textColor = UIColor.gray
    pacienteNascTextBox.layoutMargins = UIEdgeInsets.init(top: 0.0,left: 0.0,bottom: 0.0,right: 0.0)
    pacienteNascTextBox.delegate = self

    pacienteTelefoneTextBox.text = placeholderTelPac
    pacienteTelefoneTextBox.textAlignment = NSTextAlignment.center
    pacienteTelefoneTextBox.textColor = UIColor.gray
    pacienteTelefoneTextBox.layoutMargins = UIEdgeInsets.init(top: 0.0,left: 0.0,bottom: 0.0,right: 0.0)
    pacienteTelefoneTextBox.delegate = self

    procedimentoHonorarios.text = placeholderProcHonor
    procedimentoHonorarios.textAlignment = NSTextAlignment.center
    procedimentoHonorarios.textColor = UIColor.gray
    procedimentoHonorarios.layoutMargins = UIEdgeInsets.init(top: 0.0,left: 0.0,bottom: 0.0,right: 0.0)
    procedimentoHonorarios.delegate = self

    procedimentoAnatomoObs.text = placeholderProcObs
    procedimentoAnatomoObs.textAlignment = NSTextAlignment.center
    procedimentoAnatomoObs.textColor = UIColor.gray
    procedimentoAnatomoObs.layoutMargins = UIEdgeInsets.init(top: 0.0,left: 0.0,bottom: 0.0,right: 0.0)
    procedimentoAnatomoObs.delegate = self


  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    self.activeTextField = textField
    
    if textField.tag == 1 {
      textField.text = (textField.text == placeholderNomePac ? "" : textField.text)
      textField.textColor = UIColor.black
      textField.textAlignment = NSTextAlignment.center
    }
    
    if textField.tag == 2 {
      textField.text = (textField.text == placeholderTelPac ? "" : textField.text)
      textField.textColor = UIColor.black
      textField.textAlignment = NSTextAlignment.center
    }

    if textField.tag == 3 {
      textField.text = (textField.text == placeholderEmailPac ? "" : textField.text)
      textField.textColor = UIColor.black
      textField.textAlignment = NSTextAlignment.center
    }

    if textField.tag == 4 {
      textField.text = (textField.text == placeholderNascPac ? "" : textField.text)
      textField.textColor = UIColor.black
      textField.textAlignment = NSTextAlignment.center
    }
    
    if textField.tag == 5 {
      textField.text = (textField.text == placeholderProcHonor ? "" : textField.text)
      textField.textColor = UIColor.black
      textField.textAlignment = NSTextAlignment.center
    }

    if textField.tag == 6 {
      textField.text = (textField.text == placeholderProcObs ? "" : textField.text)
      textField.textColor = UIColor.black
      textField.textAlignment = NSTextAlignment.center
    }
  }
  
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    self.activeTextField = textField
    
    if textField.tag == 1 && textField.text == "" {
      textField.text = placeholderNomePac
      textField.textColor = UIColor.lightGray
      textField.textAlignment = NSTextAlignment.center
    }
    if textField.tag == 2 && textField.text == "" {
      textField.text = placeholderTelPac
      textField.textColor = UIColor.lightGray
      textField.textAlignment = NSTextAlignment.center
    }
    if textField.tag == 3 && textField.text == "" {
      textField.text = placeholderEmailPac
      textField.textColor = UIColor.lightGray
      textField.textAlignment = NSTextAlignment.center
    }
    if textField.tag == 4 && textField.text == "" {
      textField.text = placeholderNascPac
      textField.textColor = UIColor.lightGray
      textField.textAlignment = NSTextAlignment.center
    }
    if textField.tag == 5 && textField.text == "" {
      textField.text = placeholderProcHonor
      textField.textColor = UIColor.lightGray
      textField.textAlignment = NSTextAlignment.center
    }
    if textField.tag == 6 && textField.text == "" {
      textField.text = placeholderProcObs
      textField.textColor = UIColor.lightGray
      textField.textAlignment = NSTextAlignment.center
    }
  }
  
  // MARK: - Close Keyboard on Return key.
  func textFieldShouldReturn(_ userText: UITextField) -> Bool {
    userText.resignFirstResponder()
    return true
  }
  
  
  // MARK: - PickerView Delegates
  
  // how many component (i.e. column) to be displayed within picker view
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  // How many rows are there is each component
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return self.procedimentos.count
  }
  
  func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
    return 50.0
  }
  
//  func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
//    let label = UILabel(frame: CGRect(
//      x: 0, //self.procedimentoPicker.frame.origin.x,
//      y: 0, //self.procedimentoPicker.frame.origin.y,
//      width: 400, //self.procedimentoPicker.frame.size.width,
//      height: 44 ))
////    label.lineBreakMode = .byWordWrapping;
////    label.numberOfLines = 0;
//    label.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20.0)
//    label.textColor = UIColor.black
//    label.text = self.procedimentos[row]
//    label.sizeToFit()
//    // view!.addSubview(label)
//    return label;
//  }
  
//  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//    var title: String = self.procedimentos[row].procedimento
//    if self.selectedRow > 0 {
//      title = self.procedimentos[self.selectedRow].procedimento
//    }
//    return title
//  }

  
//  func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
//
//    var titleText: String = self.procedimentos[row].procedimento
//    if self.selectedRow > 0 {
//      titleText = self.procedimentos[self.selectedRow].procedimento
//    }
//
//    let attributedTitle = NSAttributedString(
//      string: titleText,
//      attributes: [
//        NSFontAttributeName:UIFont(
//          name: "HelveticaNeue-CondensedBold",
//          size: 12.0
//        )!,
//        NSForegroundColorAttributeName:UIColor.blue
//      ]
//    )
//
//    return attributedTitle
//  }
// 
  func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
    let pickerLabel = UILabel()
    var titleText: String = self.procedimentos[row].procedimento
    if self.selectedRow > 0 {
      titleText = self.procedimentos[self.selectedRow].procedimento
    }
    let title = NSAttributedString(string: titleText, attributes: [
      NSFontAttributeName:UIFont(
        name: "HelveticaNeue-CondensedBold",
        size: 18.0)!,
      NSForegroundColorAttributeName:UIColor.black])
    pickerLabel.sizeToFit()
    pickerLabel.textAlignment = .center
    pickerLabel.lineBreakMode = .byWordWrapping;
    pickerLabel.numberOfLines = 0;
    pickerLabel.attributedText = title
    return pickerLabel
  }

 
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    buildDetalhes(item: procedimentos[row])
  }
  
  func buildDetalhes(item: Procedimento) {
    var procObs: String = ""
    let tipo = item.tipo
    let duracao = item.duracao
    let valor = item.valor
    let materiais = item.detalhes.joined(separator: ", \n")
    
    procObs = "Tipo: \(tipo)\n"
    procObs = procObs + "Duração: \(duracao)min.\n"
    procObs = procObs + "Valor: \(valor)\n"
    procObs = procObs + "Materiais: \n \(materiais)"
    self.procedimentoObs.text = procObs
  }
  
  //  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
  //
  //    //        // In case need to to disable OK
  //    //        //if no filter selected.
  //    //        if row == 0 {
  //    //            self.okButton.isEnabled = false
  //    //        }else{
  //    //            self.okButton.isEnabled = true
  //    //        }
  //    //
  //    //      // Filter at 0 means NORMAL photo. No filter.
  //
  //    // call funtion to apply the selected filter
  //    self.procedimentoObs.text = procedimentos[row].detalhes.joined(separator: ", \n")
  //
  //  }
  
  //    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
  //        return 1
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
  
  
}
