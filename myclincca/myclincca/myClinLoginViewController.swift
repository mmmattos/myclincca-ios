//
//  myClinLoginViewController.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 02/03/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import Foundation
import UIKit
import KeychainAccess


class LoginViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
  
  var activeTextField = UITextField()
  let placeholderPW = "Informe sua senha aqui."
  let placeholderEmail = "Informe aqui o seu endereço de email."
  let dontHaveAnAccount = "http://www.myclincca.com.br/signup"
  
  private let SCREEN_SIZE = UIScreen.main.bounds
  private let LOGINBOX_HEIGHT_FACTOR:CGFloat = 667.0/230.0
  private let LOGINBOX_MIN_HEIGHT:CGFloat = 320.0
  
    @IBOutlet weak var logoImg: UIImageView!
  
  @IBOutlet var LoginBox: UIView!
  
  @IBOutlet var emailLabel: UILabel!
  @IBOutlet var emailTextBox: UITextField!
  @IBOutlet var pwLabel: UILabel!
  @IBOutlet var pwTextBox: UITextField!
  
  @IBOutlet var termaLabel: UILabel!
  
  
  @IBOutlet var txtDontHaveAnAccount: UITextView!
  
  
  override func viewDidLoad() {
    
    LoginBox.layer.cornerRadius = 16
    LoginBox.layer.masksToBounds = true  // optional
    var desiredHeight :CGFloat = SCREEN_SIZE.height / LOGINBOX_HEIGHT_FACTOR
    desiredHeight = (desiredHeight < LOGINBOX_MIN_HEIGHT ? LOGINBOX_MIN_HEIGHT : desiredHeight)
    let boxY = logoImg.frame.size.height + 30.0
    LoginBox.frame.origin.y = boxY
    LoginBox.frame.size.height = desiredHeight
    
    //let heightOfSubView = SCREEN_SIZE.height / 2 - SCREEN_SIZE.height * GAP_BETWEEN_VIEWS/2
    //let widthOfSubView = SCREEN_SIZE.width / 2 - SCREEN_SIZE.height * GAP_BETWEEN_VIEWS/2
    
    
    
    
    emailTextBox.text = placeholderEmail
    emailTextBox.textAlignment = NSTextAlignment.center
    emailTextBox.textColor = UIColor.gray
    emailTextBox.layoutMargins = UIEdgeInsets.init(top: 0.0,left: 0.0,bottom: 0.0,right: 0.0)
    emailTextBox.tag = 3
    emailTextBox.delegate = self
    
    
    // This field will always show the placeholder initially.
    // Dont need to garble text at this moment.
    //
    pwTextBox.text = placeholderPW
    pwTextBox.isSecureTextEntry = false
    pwTextBox.textAlignment = NSTextAlignment.center
    pwTextBox.textColor = UIColor.gray
    pwTextBox.layoutMargins = UIEdgeInsets.init(top: 0.0,left: 0.0,bottom: 0.0,right: 0.0)
    pwTextBox.tag = 4
    self.pwTextBox.delegate = self
    
    termaLabel.textAlignment = NSTextAlignment.center
    //termaLabel.textColor = UIColor.gray
    
    super.viewDidLoad()
  }
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        
        self.dismiss(animated: true)
    }
    
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(true)
    
    self.txtDontHaveAnAccount.delegate = self
    
    let str = "Não tem uma conta? Cadastre-se aqui."
    
    let attributedString = NSMutableAttributedString(string: str)
    
    let style = NSMutableParagraphStyle()
    style.alignment = NSTextAlignment.center
    
    let multipleAttributes = [
      NSForegroundColorAttributeName: UIColor.darkGray,
      NSBackgroundColorAttributeName: UIColor.clear,
      NSParagraphStyleAttributeName: style
    ]
    
    attributedString.addAttributes(multipleAttributes, range: NSMakeRange(0, str.characters.count) )
    
    
    let foundRange = attributedString.mutableString.range(of: "Cadastre-se aqui")
    attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkText, range: foundRange)
    attributedString.addAttribute(NSLinkAttributeName, value: dontHaveAnAccount, range: foundRange)
    
    txtDontHaveAnAccount.attributedText = attributedString
    
    
    
    let keychain = Keychain(service: "com.myclincca.myclincca-token")
    
    
    Utilities.Log (
      message:"Try to get session token from keychain.",
      #file,
      Utilities.typeName(some:self),
      #function,
      #line)
    
    // Get session from keychain.
    do {
      if try keychain.contains(Constants.KeychainKeys.SessionTokenKey) {
        let session_token = try keychain.get(Constants.KeychainKeys.SessionTokenKey)
        if session_token != nil {
          Utilities.Log (
            message:"Got session token: \(session_token)",
            #file,
            Utilities.typeName(some:self),
            #function,
            #line)
          
          let appDelegate = UIApplication.shared.delegate! as! AppDelegate
          let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "myclinccaMenu")
          appDelegate.window?.rootViewController = initialViewController
          appDelegate.window?.makeKeyAndVisible()
          
          return
        }
      }
    }
    catch let error {
      Utilities.Log(
        message: "\(error) ",
        #file,
        Utilities.typeName(some: self),
        #function,
        #line)
    }
    
  }
  
  
  
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    
    self.activeTextField = textField
    
    let howmuchMove: CGFloat = (textField.tag == 6 ? 150 : 100)
    
    animateViewMoving(up: true, moveValue: howmuchMove)
    
    if textField.tag == 3 || textField.tag == 6 {
      textField.text = (textField.text == placeholderEmail ? "" : textField.text)
      textField.textColor = UIColor.black
      textField.textAlignment = NSTextAlignment.center
      
    }
    
    if textField.tag == 4 {
      textField.isSecureTextEntry = (textField.text == placeholderPW ? true : false)
      textField.text = (textField.text == placeholderPW ? "" : textField.text)
      textField.textColor = UIColor.black
      textField.textAlignment = NSTextAlignment.center
      textField.isSecureTextEntry = true
      
    }
    
  }
  
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    self.activeTextField = textField
    let howmuchMove: CGFloat = (textField.tag == 6 ? 150 : 100)
    animateViewMoving(up: false, moveValue: howmuchMove)
    
    if textField.tag == 3 && textField.text == "" {
      
      textField.text = placeholderEmail
      textField.textColor = UIColor.lightGray
      textField.textAlignment = NSTextAlignment.center
      
    }
    
    if textField.tag == 4 && textField.text == "" {
      textField.text = placeholderPW
      textField.isSecureTextEntry = false
      textField.textColor = UIColor.lightGray
      textField.textAlignment = NSTextAlignment.center
    }
  }
  
  
  // MARK: - Move the view up if keyboard overlaps text fields.
  func animateViewMoving (up:Bool, moveValue :CGFloat){
    let movementDuration:TimeInterval = 0.3
    let movement:CGFloat = ( up ? -moveValue : moveValue)
    UIView.beginAnimations( "animateView", context: nil)
    UIView.setAnimationBeginsFromCurrentState(true)
    UIView.setAnimationDuration(movementDuration )
    self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
    UIView.commitAnimations()
  }
  
  
  // MARK: - Close Keyboard on Return key.
  func textFieldShouldReturn(_ userText: UITextField) -> Bool {
    userText.resignFirstResponder()
    return true
  }
  
  /**
   Clear input fields.
   */
  func clearFields()
  {
    emailTextBox.text = "";
    pwTextBox.text = "";
    return;
    
  }
  
  //    @IBAction func alreadyUserButtonTapped(sender: Any) {
  //        self.dismiss(animated: true, completion: nil)
  //        return
  //    }
  
  func tappedOnCreateAccount( url: URL) -> Bool {
    if (url.absoluteString == dontHaveAnAccount) {
      
      Utilities.Log (
        message:"Tapped on CREATE ACCOUNT, switching vc to RegisterViewController!",
        #file,
        Utilities.typeName(some:self),
        #function,
        #line)
      
      //Perform segue to map.
      let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
      let vc : RegisterViewController = mainStoryboard
        .instantiateViewController(
          withIdentifier: "RegisterVC") as! RegisterViewController
      vc.cleanUpTempRegisterStore()
      self.present(vc, animated: true, completion: nil)
      
    }
    return false
  }
  
  //iOS 7...9
  @available(iOS, deprecated: 10.0)
  func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
    
    return tappedOnCreateAccount(url: url)
    
  }
  
  // iOS 10+
  @available(iOS 10.0, *)
  func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange,
                interaction: UITextItemInteraction) -> Bool {
    
    return tappedOnCreateAccount(url: url)
  }
  
  @IBAction func loginButtonTapped(_ sender: Any) {
    
    let userEmail = (emailTextBox.text ?? "")
    let userPassword = (pwTextBox.text ?? "")
    
    
    //Validate fields
    if (userEmail.isEmpty || userPassword.isEmpty)
    {
      mmmattosAlerts.showAlert(
        viewController: self as UIViewController,
        style: .alert,
        message: "Todos os campos são obrigatórios!",
        completion: nil,
        buttons: (.default, "Ok", nil)
      )
      return;
    }
    
    
    // MARK: - Login the user
    ApiController.doLogin(username: userEmail,password: userPassword)
    {
      (str, error) in
      if (str != nil) {
        
        print("Returned form Login API:  \(String(describing: str))")
//        mmmattosAlerts.showAlert(
//          viewController: self as UIViewController,
//          style: .alert,
//          message: str!,
//          completion: {
//            action in
              self.resetLoginFields()
              //Perform segue.
              let appDelegate = UIApplication.shared.delegate! as! AppDelegate
              let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "myclinccaMenu") as! myClinMenuViewController
              appDelegate.window?.rootViewController = initialViewController
              initialViewController.currentUserLabel.text = "Olá \(str!)"
              appDelegate.window?.makeKeyAndVisible()
              self.navigationController?.popViewController(
                animated: true
              )
              self.dismiss(animated: true, completion: nil)
              return
//          },
//          buttons: (.default, "Ok", nil)
//        )
//        return
      }
      var message = "Ocorreu um erro no login do usuário. Tente mais tarde!"
      if  error !=  nil {
        
        let statusCode = error!["statusCode"] as! Int
        let errorMessage = error!["message"] as! String
        
        switch statusCode {
        case NSURLErrorCannotFindHost:
          message = "Não foi possível conectar ao servidor de autenticação. Tente mais tarde."
          break
        case NSURLErrorNotConnectedToInternet:
          message = "Conexão com a Internet indisponível"
          break
        case NSURLErrorCannotConnectToHost:
          message = "Não foi possível conectar com o servidor MyClin.cca."
          break
        case 401:
          if errorMessage != "" {
            message = errorMessage
          } else {
            message = "Email ou Senha informados inválidos. Tente novamente."
          }
          break
        case 404:
          if errorMessage != "" {
            message = errorMessage
          } else {
            message = "Usuário não cadastrado"
          }
          break
        case 500:
          message = "Foi detectado um erro não tratado (500)."
          break
        default:
          message = "\(message) (\(statusCode))"
          break
        }
      }
      mmmattosAlerts.showAlert(
        viewController: self as UIViewController,
        style: .alert,
        message: message,
        completion: nil,
        buttons: (.default, "Ok", nil)
      )
      return;
    }
  }
  
  func resetLoginFields() {
    self.emailTextBox.text = "";
    self.pwTextBox.text = "";
    self.navigationController?.popViewController(animated: true)
    self.dismiss(animated: true, completion: nil)
  }
  
  func translateLoginErrorCode(status :Int, message: String) -> String {
    
    var finalMessage = "Login Error. "
    
    print ("MESSAGE: \(message) - \(status)")
    
    switch status {
    case 404:
      if message == "GOTNOSESSIONINFOFROMCDB" {
        finalMessage = finalMessage + "Não foi possível obter o token para o Aplicativo."
      } else {
        finalMessage = finalMessage + "Este usuário não tem uma conta! Cadastre-se e crie uma nova conta!"
      }
      break;
    case 401:
      if message != "" {
        finalMessage = finalMessage + message
      } else {
        finalMessage = finalMessage + "Email ou senha inválidos. Por favor, tente novamente!"
      }
      break;
    case 400:
      if message == "ERRORGETTINGTOKENFROMCDB" {
        finalMessage = finalMessage + "Não foi possível recuperar a sessão do usuarion. Por favor, tente novamente mais tade!"
      } else {
        finalMessage = finalMessage + "Por favor, tente novamente."
      }
      break;
    case 500:
      finalMessage = finalMessage + "Um erro não identificado foi detectado. Por favor, tente novamente."
      break;
    default:
      finalMessage = finalMessage + "Por favor, tente novamente."
      break;
      
    }
    
    return finalMessage
    
  }
  
  
}
