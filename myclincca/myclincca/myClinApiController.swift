
//  ApiController.swift
//
//  Created by Miguel Miranda de Mattos.
//

import Foundation
import Alamofire
import SwiftyJSON
//import Alamofire_SwiftyJSON
import KeychainAccess


enum Router: URLRequestConvertible{
  
  //static var baseURLString: String = NSUserDefaults.standardUserDefaults().objectForKey("url") as String!
  static let keychain = Keychain(service: "com.myclincca.myclincca-token")
  static let baseURLString = Constants.AppUrls.ApiFinalUrl
  static let authURLString = Constants.AppUrls.AuthApiFinalUrl
  static var OAuthToken: String? = try! keychain.get("myclincca-token")
  
  case Markers([String: AnyObject])
  case SaveMarker([String: AnyObject])
  case DoLogin([String: AnyObject])
  case DoRegister([String: AnyObject])
  case CreatePost([String: AnyObject])
  case GetMockedContents()
  case GetEnlaces()
  case GetDuvidas()
  case GetTipoProcedimentos()
  case GetTipoProcedimentosByTipo(String)
  case GetDepoimentos()
  case SecoesClinica()
  case GetAgenda(String)
  case GetProcedimentos()
  case ConfirmaAgendamento([String: AnyObject])
  case CancelaAgendamento([String: AnyObject])
  
  var method: Alamofire.HTTPMethod {
    switch self {
    case .Markers:
      return .get
    case .SaveMarker:
      return .post
    case .CreatePost:
      return .post
    case .GetMockedContents:
      return .get
    case .DoLogin:
      return .post
    case .DoRegister:
      return .post
    case .SecoesClinica:
      return .get
    case .GetDepoimentos:
      return .get
    case .GetTipoProcedimentos:
      return .get
    case .GetTipoProcedimentosByTipo:
      return .get
    case .GetDuvidas:
      return .get
    case .GetEnlaces:
      return .get
    case .GetAgenda:
      return .get
    case .GetProcedimentos:
      return .get
    case .ConfirmaAgendamento:
      return .post
    case .CancelaAgendamento:
      return .post
      //    default:
      //      return .get
    }
  }
  
  var path: String {
    switch self {
    case .Markers:
      return "/markers"
    case .SaveMarker:
      return "/markers"
    case .CreatePost:
      return "/contents"
    case .GetMockedContents:
      return "/contents/mocked"
    case .DoLogin:   //(let username, let password):
      return "/auth/mobile/local"
    case .DoRegister:
      return "/users/mobile"
    case .SecoesClinica:
      return "/clinicas"
    case .GetDepoimentos:
      return "/depoimentos"
    case .GetDuvidas:
      return "/duvidas"
    case .GetTipoProcedimentos:
      return "/tipoprocedimentos"
    case .GetTipoProcedimentosByTipo(let tipo):
      return "/tipoprocedimentos/mobile/dotipo/\(tipo)"
    case .GetEnlaces:
      return "/enlaces"
    case .GetAgenda(let dataAgenda):
      return "/agendaprocedimentos/mobile/dodia/\(dataAgenda)"
    case .GetProcedimentos:
      return "/procedimentos/mobile/ativos"
    case .ConfirmaAgendamento:
      return "/agendaprocedimentos/mobile/confirma"
    case .CancelaAgendamento:
      return "/agendaprocedimentos/mobile/cancela"
      //    default:
      //      return ""
    }
  }
  
  // MARK: URLRequestConvertible
  
  func asURLRequest() throws -> URLRequest {
    
    var url: URL
    switch self {
    case .DoLogin:
      url  =  URL(string: Router.authURLString)!
    default:
      url = URL(string: Router.baseURLString)!
    }
    
    var urlRequest = URLRequest(url: url.appendingPathComponent(path))
    urlRequest.httpMethod = method.rawValue
    
    if let token = Router.OAuthToken {
      Utilities.Log (
        message:"TOKEN IN REQUEST: \(token) ",
        #file,
        Utilities.typeName(some: self),
        #function,
        #line
      )
      urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
    }
    
    switch self {
    case .Markers(let parameters):
      return try Alamofire.JSONEncoding.default.encode(urlRequest, with: parameters)
    case .SaveMarker(let parameters):
      return try Alamofire.JSONEncoding.default.encode(urlRequest, with: parameters)
    case .CreatePost(let parameters):
      return try Alamofire.JSONEncoding.default.encode(urlRequest, with: parameters)
    case .GetMockedContents():
      return try Alamofire.JSONEncoding.default.encode(urlRequest, with: nil)
    case .DoLogin(let parameters):
      return try Alamofire.JSONEncoding.default.encode(urlRequest, with: parameters)
    case .DoRegister(let parameters):
      return try Alamofire.JSONEncoding.default.encode(urlRequest, with: parameters)
    case .SecoesClinica():
      return try Alamofire.JSONEncoding.default.encode(urlRequest, with: nil )
    case .GetDepoimentos():
      return try Alamofire.JSONEncoding.default.encode(urlRequest, with: nil )
    case .GetDuvidas():
      return try Alamofire.JSONEncoding.default.encode(urlRequest, with: nil )
    case .GetTipoProcedimentos():
      return try Alamofire.JSONEncoding.default.encode(urlRequest, with: nil )
    case .GetEnlaces():
      return try Alamofire.JSONEncoding.default.encode(urlRequest, with: nil )
    case .GetProcedimentos():
      return try Alamofire.JSONEncoding.default.encode(urlRequest, with: nil)
    case .ConfirmaAgendamento(let parameters):
      return try Alamofire.JSONEncoding.default.encode(urlRequest, with: parameters)
    case .CancelaAgendamento(let parameters):
      return try Alamofire.JSONEncoding.default.encode(urlRequest, with: parameters)
    //case .GetTipoProcedimentosByTipo(let tipo):
    //  return try Alamofire.JSONEncoding.default.encode(urlRequest, with: tipo)
    //case .GetAgenda(let dataAgenda):
    //  return try Alamofire.JSONEncoding.default.encode(urlRequest, with: dataAgenda)
    default:
      return urlRequest
    }
  }
}



class ApiController {
  
  
  /**
   Login method using managed services and stores token on keychain
   @param username
   @param password
   @returns json object containing an user and an access_token.
   */
  class func doLogin(
    username: String,
    password: String,
    completionHandler: @escaping (String?, NSMutableDictionary?) -> ()) -> ()
  {
    
    let keychain = Keychain(service: "com.myclincca.myclincca-token")
    //.accessibility(.WhenUnlocked)
    
    // We'll use the email as the login id.
    //TODO: At some point identify if this is an email or username and use appropriately.
    
    let user = User (
      name: "",
      email: username,
      password: password,
      especialidade: "",
      cremers: "",
      telefone: "",
      rg: "",
      cpf: "",
      estadoCivil: "",
      endereco: "",
      cidade: "",
      estado: ""
    )
    
    SessionManager.default.request(
      Router.DoLogin(
        jsonResponse(data: user.toJson()!)
      )
      )
      .validate(statusCode: 200..<600)
      .responseJSON { response in
        
        Utilities.Log (
          message:"All Headers: \(response) ",
          #file,
          Utilities.typeName(some: self),
          #function,
          #line
        )
        switch response.result {
        case .success :
          if let statusCode = response.response?.statusCode {
            if  statusCode >= 200 && statusCode < 400 {
              if let jsonData = response.result.value {
                let json = JSON(jsonData)
                Utilities.Log(
                  message:"Response JSON: \(json)",
                  Utilities.typeName(some:self),
                  #file,
                  #function,
                  #line
                )
                if let userName = json["userName"].stringValue as String? {
                  NSLog ("ApiController:DoLogin:UserName: \(userName) ")
                  if let userEmail = json["userEmail"].stringValue as String? {
                    NSLog ("ApiController:DoLogin:UserName: \(userEmail) ")
                    if let token = json["token"].stringValue as String? {
                      NSLog("ApiController:DoLogin:Session ID: \(token)")
                      if let userId = json["userId"].stringValue as String? {
                        do {
                          try keychain.set(
                            token as String,
                            key: Constants.KeychainKeys.SessionTokenKey)
                          try keychain.set(
                            userId as String,
                            key: Constants.KeychainKeys.SessionUserId)
                          try keychain.set(
                            userName as String,
                            key: Constants.KeychainKeys.SessionUserName)
                          try keychain.set(
                            userEmail as String,
                            key: Constants.KeychainKeys.SessionUserEmail)
                          completionHandler(userName, nil)
                        }
                        catch let error {
                          Utilities.Log(
                            message: "\(error)",
                            #file,
                            Utilities.typeName(some: self),
                            #function,
                            #line)
                        }   // catch
                      }     // userId
                    }         // token
                  }         // userEmail
                }           // userName
              }             // jsonData
            }                 // end of codes 200..<400
            if statusCode > 400 {
              //NSLog("ApiController:DoLogin:LoginPageViewController: Error: \(error) ")
              if let headersData = response.result.value {
                let headers = JSON(headersData);
                if let message = headers["message"].string {
                  let errorReportData: NSMutableDictionary = NSMutableDictionary()
                  errorReportData.setValue(statusCode, forKey: "statusCode")
                  errorReportData.setValue(message, forKey:"message")
                  print("ErrorReportData: \(errorReportData)")
                  completionHandler(nil, errorReportData)
                }
                else
                {
                  let errorReportData: NSMutableDictionary = NSMutableDictionary()
                  errorReportData.setValue(statusCode, forKey: "statusCode")
                  errorReportData.setValue("", forKey:"message")
                  completionHandler(nil, errorReportData)
                }
              }
            }                             // end of statusCodes > 400
          }
        case .failure(let error) :
          if let error = error as? NSError, error.domain == NSURLErrorDomain  {
            var message = ""
            var statusCode = 0
            switch error.code {
            case NSURLErrorNotConnectedToInternet:
              statusCode = error.code
              message = "NOINTERNET"
            case NSURLErrorCannotFindHost:
              statusCode = NSURLErrorCannotFindHost
              message = "HOSTUNREACHABLE"
            case NSURLErrorCannotConnectToHost:
              statusCode = NSURLErrorCannotConnectToHost
              message = "CANTCONNECT2HOST"
              break
            default:
              statusCode = 500
              message = "UNHANDLEDERROR"
              
            }
            let errorReportData: NSMutableDictionary = NSMutableDictionary()
            errorReportData.setValue(statusCode, forKey: "statusCode")
            errorReportData.setValue(message, forKey:"message")
            completionHandler(nil, errorReportData)
          }
          if let statusCode = response.response?.statusCode {
            if let headersData = response.response?.allHeaderFields {
              let headers = JSON(headersData);
              if let message = headers["message"].string {
                
                let errorReportData: NSMutableDictionary = NSMutableDictionary()
                errorReportData.setValue(statusCode, forKey: "statusCode")
                errorReportData.setValue(message, forKey:"message")
                errorReportData.setValue(error, forKey:"error")
                completionHandler(nil, errorReportData)
              }
              else
              {
                let errorReportData: NSMutableDictionary = NSMutableDictionary()
                errorReportData.setValue(statusCode, forKey: "statusCode")
                errorReportData.setValue("", forKey:"message")
                errorReportData.setValue(error, forKey:"error")
                completionHandler(nil, errorReportData)
              }
            }
          }
          
        default :
          Utilities.Log(
            message:"Unhandled Error Response JSON: \(response)",
            Utilities.typeName(some: self),
            #file,
            #function,
            #line
          )
          break
        } // switch...
    }
  }
  
  
  class func getProcedimentos(
    completionHandler: @escaping ([Procedimento]?, NSMutableDictionary?) -> ()) -> ()
  {
    var procedimentos: [Procedimento] = []
    SessionManager.default.request(
      Router.GetProcedimentos()
      )
      .validate(statusCode: 200..<300)
      .responseJSON { response in
        
        if (response.response?.statusCode) != nil {
          let statusCode = response.response?.statusCode
          switch response.result {
          case .success:
            if let jsonData = response.result.value {
              let json = JSON(jsonData)
              let posts = json
              for index in 0..<(posts.count) {
                let jsonPost = posts.arrayValue[index]
                
                var id = ""
                if let i = jsonPost["_id"].string {
                  id = i
                }
                var ativo = false
                if let at = jsonPost["ativo"].bool {
                  ativo = at
                }
                var procedimentoTitle = ""
                if let p = jsonPost["procedimento"].string {
                  procedimentoTitle = p
                }
                var descricao = ""
                if let d = jsonPost["descicao"].string {
                  descricao = d
                }
                var tipo = ""
                if let t = jsonPost["tipo"].string {
                  tipo = t
                }
                var valor = ""
                if let v = jsonPost["valor"].string {
                  valor = v
                }
                var duracao = 0.0
                if let du = jsonPost["duracao"].double {
                  duracao = du
                }
                
                let detalhes:[String] = jsonPost["detalhes"].arrayValue.map { $0.stringValue}
                
                let procedimento = Procedimento(
                  procedimento: procedimentoTitle,
                  descricao: descricao,
                  duracao: duracao,
                  tipo: tipo,
                  detalhes: detalhes,
                  valor: valor,
                  ativo: ativo,
                  id: id
                )
                procedimentos.append(procedimento)
              }
            }
            completionHandler(procedimentos, nil)
          case .failure(let error):
            if let headersData = response.response?.allHeaderFields {
              let headers = JSON(headersData);
              if let message = headers["message"].string {
                
                let errorReportData: NSMutableDictionary = NSMutableDictionary()
                errorReportData.setValue(statusCode, forKey: "statusCode")
                errorReportData.setValue(message, forKey:"message")
                errorReportData.setValue(error, forKey:"error")
                completionHandler(nil, errorReportData)
              }
              else
              {
                let errorReportData: NSMutableDictionary = NSMutableDictionary()
                errorReportData.setValue(statusCode, forKey: "statusCode")
                errorReportData.setValue("", forKey:"message")
                errorReportData.setValue(error, forKey:"error")
                completionHandler(nil, errorReportData)
              }
            } // end of failure case.
          } // end of switch
        }
    }
  }
  
  
  
  class func getAgenda(
    dataP: String,
    completionHandler: @escaping ([HorarioAgenda]?, NSMutableDictionary?) -> ()) -> ()
  {
    var horarios: [HorarioAgenda] = []
    
    
    SessionManager.default.request(
      Router.GetAgenda(dataP)
      )
      .validate(statusCode: 200..<300)
      .responseJSON { response in
        
        if (response.response?.statusCode) != nil {
          let statusCode = response.response?.statusCode
          switch response.result {
          case .success:
            if let jsonData = response.result.value {
              let json = JSON(jsonData)
              //NSLog("ApiController:DoLogin:Response:JSON: \(response) ")
              //NSLog("ApiController:SecoesClinica:Response:JSON: \(json)")
              //print(jsonData)
              
              let posts = json
              for index in 0..<(posts.count) {
                let jsonPost = posts.arrayValue[index]
                //                Utilities.Log(
                //                  message:"Register Payload JSON: \(String(describing: jsonPost))",
                //                  Utilities.typeName(some: self),
                //                  #file,
                //                  #function,
                //                  #line
                //                )
                
                
                var id = ""
                if let i = jsonPost["_id"].string {
                  id = i
                }
                var ativo = false
                if let at = jsonPost["ativo"].bool {
                  ativo = at
                }
                var disponivel = false
                if let disp = jsonPost["disponivel"].bool {
                  disponivel = disp
                }
                var autorizado = false
                if let aut = jsonPost["autorizado"].bool {
                  autorizado = aut
                }
                var horario = ""
                if let slot = jsonPost["horario"].string {
                  horario = slot
                }
                var emailAutor = ""
                if let email = jsonPost["emailAutor"].string {
                  emailAutor = email
                }
                var dataP: String = ""
                if let d = jsonPost["data"].string {
                  let index = d.index(d.startIndex, offsetBy: 10)
                  dataP = d.substring(to: index)  // YYYY-MM-DD
                  
                  // THESE BELOW MAY COME NULL
                  var user = ""
                  if let u = jsonPost["user"]["_id"].string {
                    user = u
                  }
                  var procedimento = ""
                  if let p = jsonPost["procedimento"]["_id"].string {
                    procedimento = p
                  }
                  var autorizado = false
                  if let a = jsonPost["autorizado"].bool {
                    autorizado = a
                  }
                  var pacienteNome = ""
                  var pacienteTelefone = ""
                  var pacienteEmail = ""
                  var pacienteNasc = ""
                  var procedimentoMedHonorarios = ""
                  var procedimentoAnatomoObs = ""
                  if let pa = jsonPost["pacienteNome"].string {
                    pacienteNome = pa
                    if let pt = jsonPost["pacienteTelefone"].string {
                      pacienteTelefone = pt
                    }
                    if let pe = jsonPost["pacienteEmail"].string {
                      pacienteEmail = pe
                    }
                    if let pn = jsonPost["pacienteDataNascimento"].string {
                      pacienteNasc = pn
                    }
                    if let prh = jsonPost["honorariosMedicos"].string {
                      procedimentoMedHonorarios = prh
                    }
                    if let pra = jsonPost["observacao"].string {
                      procedimentoAnatomoObs = pra
                    }

                    
                  }
                  
                  let horarioAgenda = HorarioAgenda (
                    data: dataP,
                    horario: horario,
                    emailAutor: emailAutor,
                    ativo: ativo,
                    disponivel: disponivel,
                    autorizado: autorizado,
                    pacienteNome: pacienteNome,
                    pacienteTelefone: pacienteTelefone,
                    pacienteEmail: pacienteEmail,
                    pacienteNasc: pacienteNasc,
                    procedimentoMedHonorarios: procedimentoMedHonorarios,
                    procedimentoAnatomoObs: procedimentoAnatomoObs,
                    procedimento: procedimento,
                    user: user,
                    id: id
                  )
                  
                  //print ("Horario: \(horarioAgenda.showMe())")
                  horarios.append(horarioAgenda)
                }
              }
              
              completionHandler(horarios, nil)
            }
          case .failure(let error):
            //NSLog("ApiController:GetAgenda: Error: \(error) ")
            if let headersData = response.response?.allHeaderFields {
              let headers = JSON(headersData);
              if let message = headers["message"].string {
                
                let errorReportData: NSMutableDictionary = NSMutableDictionary()
                errorReportData.setValue(statusCode, forKey: "statusCode")
                errorReportData.setValue(message, forKey:"message")
                errorReportData.setValue(error, forKey:"error")
                completionHandler(nil, errorReportData)
              }
              else
              {
                let errorReportData: NSMutableDictionary = NSMutableDictionary()
                errorReportData.setValue(statusCode, forKey: "statusCode")
                errorReportData.setValue("", forKey:"message")
                errorReportData.setValue(error, forKey:"error")
                completionHandler(nil, errorReportData)
              }
            } // end of failure case.
          } // end of switch
        }
    }
  }
  
  
  
  class func cancelaAgendamento(
    agendamentoId: String,
    reqdSlots: Double,
    completionHandler: @escaping (String?, NSMutableDictionary?) -> ()) -> ()
  {
    if agendamentoId.isEmpty || reqdSlots == 0 {
      let message = "Info requerida não encontrada: Agendamento, Nro. Horarios requeridos."
      Utilities.Log(
        message: message,
        Utilities.typeName(some: self),
        #file,
        #function,
        #line
      )
      let report = Utilities.createErrorReportData(
        code: 400,
        message: message
      )
      completionHandler(nil, report)
    }
    
    let pedidoCancelamento: JSON = [
      "agendamentoId": agendamentoId,
      "reqdSlots": reqdSlots,
      ]
    
    do  {
      let pCancData = try pedidoCancelamento.rawData() as NSData
      SessionManager.default.request(
        Router.CancelaAgendamento(
          jsonResponse(
            data: pCancData
          )
        )
        )
        .validate(statusCode: 200..<600)
        .responseJSON { response in
          switch response.result {
          case .success:
            if let statusCode = response.response?.statusCode {
              if statusCode >= 400 {
                if let headersData = response.result.value {
                  let headers = JSON(headersData);
                  if let message = headers["message"].string {
                    let errorReportData: NSMutableDictionary = NSMutableDictionary()
                    errorReportData.setValue(statusCode, forKey: "statusCode")
                    errorReportData.setValue(message, forKey:"message")
                    print("ErrorReportData: \(errorReportData)")
                    completionHandler(nil, errorReportData)
                    return
                  }
                  else
                  {
                    let errorReportData: NSMutableDictionary = NSMutableDictionary()
                    errorReportData.setValue(statusCode, forKey: "statusCode")
                    errorReportData.setValue("", forKey:"message")
                    completionHandler(nil, errorReportData)
                    return
                  }
                }
                completionHandler(nil, nil)
                return
              }
            }
            if let jsonData = response.result.value {
              let json = JSON(jsonData)
              Utilities.Log(
                message:"Response JSON: \(json)",
                Utilities.typeName(some: self),
                #file,
                #function,
                #line
              )
              let str = json["message"].string       // str will be nil if info is nil
              completionHandler(str, nil)
              return
            }
          case .failure(let error):
            if let headersData = response.response?.allHeaderFields {
              let headers = JSON(headersData);
              print(headers)
              let statusCode = headers["statusCode"].string
              if let message = headers["message"].string {
                
                let errorReportData: NSMutableDictionary = NSMutableDictionary()
                errorReportData.setValue(statusCode, forKey: "statusCode")
                errorReportData.setValue(message, forKey:"message")
                errorReportData.setValue(error, forKey:"error")
                completionHandler(nil, errorReportData)
              }
              else
              {
                let errorReportData: NSMutableDictionary = NSMutableDictionary()
                errorReportData.setValue(statusCode, forKey: "statusCode")
                errorReportData.setValue("", forKey:"message")
                errorReportData.setValue(error, forKey:"error")
                completionHandler(nil, errorReportData)
              }
            }
          }
      }
    } catch let someJSONError {
      Utilities.Log(
        message:"JSON Error: \(someJSONError)",
        Utilities.typeName(some:self),
        #file,
        #function,
        #line
      )
    }
    completionHandler(nil, nil)
  }
  
  
  
  class func confirmaAgendamento(
    agendamentoId: String,
    medicoId: String,
    procedimentoId: String,
    pacienteNome: String,
    pacienteTelefone: String,
    pacienteEmail: String,
    pacienteNasc: String,
    medicoHonorarios: String,
    medicoAnatomoObs: String,
    reqdSlots: Double,
    completionHandler: @escaping (String?, NSMutableDictionary?) -> ()) -> ()
    // Data e Hora não necessários. Já estão no agendamento original.
  {
    if agendamentoId.isEmpty || medicoId.isEmpty || procedimentoId.isEmpty ||
      pacienteNome.isEmpty || pacienteTelefone.isEmpty || pacienteEmail.isEmpty ||
      pacienteNasc.isEmpty || medicoHonorarios.isEmpty || medicoAnatomoObs.isEmpty {
      let message = "Info requerida não encontrada: Agendamento, Medico ou Procedimento"
      Utilities.Log(
        message: message,
        Utilities.typeName(some: self),
        #file,
        #function,
        #line
      )
      let report = Utilities.createErrorReportData(
        code: 400,
        message: message
      )
      completionHandler(nil, report)
    }
    
    let pedidoConfirmacao: JSON = [
      "agendamentoId": agendamentoId,
      "user": medicoId,
      "procedimento": procedimentoId,
      "reqdSlots": reqdSlots,
      "pacienteNome":pacienteNome,
      "pacienteTelefone": pacienteTelefone,
      "pacienteEmail": pacienteEmail,
      "pacienteDataNascimento": pacienteNasc,
      "honorariosMedicos": medicoHonorarios,
      "observacao": medicoAnatomoObs
    ]
    
    do {
      
      let pConfData = try pedidoConfirmacao.rawData() as NSData
      
      SessionManager.default.request(Router.ConfirmaAgendamento(jsonResponse(data: pConfData)))
      .validate(statusCode: 200..<600)
      .responseJSON { response in
        switch response.result {
        case .success:
          if let statusCode = response.response?.statusCode {
            if statusCode >= 400 {
              //NSLog("ApiController:DoLogin:LoginPageViewController: Error: \(error) ")
              if let headersData = response.result.value {
                let headers = JSON(headersData);
                if let message = headers["message"].string {
                  let errorReportData: NSMutableDictionary = NSMutableDictionary()
                  errorReportData.setValue(statusCode, forKey: "statusCode")
                  errorReportData.setValue(message, forKey:"message")
                  print("ErrorReportData: \(errorReportData)")
                  completionHandler(nil, errorReportData)
                  return
                }
                else
                {
                  let errorReportData: NSMutableDictionary = NSMutableDictionary()
                  errorReportData.setValue(statusCode, forKey: "statusCode")
                  errorReportData.setValue("", forKey:"message")
                  completionHandler(nil, errorReportData)
                  return
                }
              }
              completionHandler(nil, nil)
              return
            }                             // end of statusCodes > 400
          }
          if let jsonData = response.result.value {
            let json = JSON(jsonData)
            
            Utilities.Log(
              message:"Response JSON: \(json)",
              Utilities.typeName(some: self),
              #file,
              #function,
              #line
            )
            
            let str = json["message"].string       // str will be nil if info is nil
            completionHandler(str, nil)
            return
            
          }
        case .failure(let error):
          if let headersData = response.response?.allHeaderFields {
            let headers = JSON(headersData);
            print(headers)
            let statusCode = headers["statusCode"].string
            if let message = headers["message"].string {
              
              let errorReportData: NSMutableDictionary = NSMutableDictionary()
              errorReportData.setValue(statusCode, forKey: "statusCode")
              errorReportData.setValue(message, forKey:"message")
              errorReportData.setValue(error, forKey:"error")
              completionHandler(nil, errorReportData)
            }
            else
            {
              let errorReportData: NSMutableDictionary = NSMutableDictionary()
              errorReportData.setValue(statusCode, forKey: "statusCode")
              errorReportData.setValue("", forKey:"message")
              errorReportData.setValue(error, forKey:"error")
              completionHandler(nil, errorReportData)
            }
          }   // end of switch
        }
    }
    }
    catch let someJSONError {
      Utilities.Log(
        message:"JSON Error: \(someJSONError)",
        Utilities.typeName(some:self),
        #file,
        #function,
        #line
      )
    }
    completionHandler(nil, nil)
    
  }
  
  /**
   User registration method using managed services and stores token on keychain
   @param username
   @param password
   @returns status
   */
  class func doRegister(
    name: String,
    email: String,
    password: String,
    especialidade: String = "",
    cremers: String = "",
    telefone: String = "",
    rg: String = "",
    cpf: String = "",
    estadoCivil:  String = "",
    endereco:  String = "",
    cidade:  String = "",
    estado:  String = "",
    completionHandler: @escaping (String?, NSMutableDictionary?) -> ()) -> ()
  {
    
    let user = User(
      name: name,
      email: email,
      password: password,
      especialidade: especialidade,
      cremers: cremers,
      telefone: telefone,
      rg: rg,
      cpf: cpf,
      estadoCivil: estadoCivil,
      endereco: endereco,
      cidade: cidade,
      estado: estado
    )
    
    Utilities.Log(
      message:"Register Payload JSON: \(String(describing: user.toJsonString()))",
      Utilities.typeName(some: self),
      #file,
      #function,
      #line
    )
    
    SessionManager.default.request(Router.DoRegister(jsonResponse(data: user.toJson()!)))
      .validate(statusCode: 200..<600)
      .responseJSON { response in
        switch response.result {
        case .success:
          if let statusCode = response.response?.statusCode {
            if  statusCode >= 200 && statusCode < 400 {
              if let jsonData = response.result.value {
                let json = JSON(jsonData)
                
                Utilities.Log(
                  message:"Response JSON: \(json)",
                  Utilities.typeName(some: self),
                  #file,
                  #function,
                  #line
                )
                
                let registrationToken = json["token"].string      // str will be nil if info is nil
                var str: String? = nil
                if registrationToken != nil {
                  str = "Usuario registrado com sucesso. Aguarde autorização da MyClin utilizar a agenda."
                }
                completionHandler(str, nil)
              }
            }
            if statusCode > 400 {
              if let headersData = response.result.value {
                let headers = JSON(headersData);
                if let message = headers["message"].string {
                  let errorReportData: NSMutableDictionary = NSMutableDictionary()
                  errorReportData.setValue(statusCode, forKey: "statusCode")
                  errorReportData.setValue(message, forKey:"message")
                  print("ErrorReportData: \(errorReportData)")
                  completionHandler(nil, errorReportData)
                }
                else
                {
                  let errorReportData: NSMutableDictionary = NSMutableDictionary()
                  errorReportData.setValue(statusCode, forKey: "statusCode")
                  errorReportData.setValue("", forKey:"message")
                  completionHandler(nil, errorReportData)
                }
              }
            }                             // end of statusCodes > 400
          }
        
        case .failure(let error):
          if let error = error as? NSError, error.domain == NSURLErrorDomain  {
            var message = ""
            var statusCode = 0
            switch error.code {
            case NSURLErrorNotConnectedToInternet:
              statusCode = error.code
              message = "NOINTERNET"
            case NSURLErrorCannotFindHost:
              statusCode = NSURLErrorCannotFindHost
              message = "HOSTUNREACHABLE"
            case NSURLErrorCannotConnectToHost:
              statusCode = NSURLErrorCannotConnectToHost
              message = "CANTCONNECT2HOST"
              break
            default:
              statusCode = 500
              message = "UNHANDLEDERROR"
              
            }
            let errorReportData: NSMutableDictionary = NSMutableDictionary()
            errorReportData.setValue(statusCode, forKey: "statusCode")
            errorReportData.setValue(message, forKey:"message")
            completionHandler(nil, errorReportData)
          }
          if let statusCode = response.response?.statusCode {
            if let headersData = response.response?.allHeaderFields {
              let headers = JSON(headersData);
              if let message = headers["message"].string {
                
                let errorReportData: NSMutableDictionary = NSMutableDictionary()
                errorReportData.setValue(statusCode, forKey: "statusCode")
                errorReportData.setValue(message, forKey:"message")
                errorReportData.setValue(error, forKey:"error")
                completionHandler(nil, errorReportData)
              }
              else
              {
                let errorReportData: NSMutableDictionary = NSMutableDictionary()
                errorReportData.setValue(statusCode, forKey: "statusCode")
                errorReportData.setValue("", forKey:"message")
                errorReportData.setValue(error, forKey:"error")
                completionHandler(nil, errorReportData)
              }
            }
          }
          
        default :
          Utilities.Log(
            message:"Unhandled Error Response JSON: \(response)",
            Utilities.typeName(some: self),
            #file,
            #function,
            #line
          )
          break
    }
  }
}



/**
 @function: doGetDepoimentos
 @abstract: Depoimentos posts related to MyClin.cca from the API
 @author about.me/mmmattos
 */
class func doGetDepoimentos(
  completionHandler: @escaping ([Depoimento]?, NSMutableDictionary?) -> ()
  ) -> ()
{
  
  var depoimentos: [Depoimento] = []
  
  SessionManager.default.request(Router.GetDepoimentos())
    .validate(statusCode: 200..<300)
    .responseJSON { response in
      
      if (response.response?.statusCode) != nil {
        let statusCode = response.response?.statusCode
        switch response.result {
        case .success:
          if let jsonData = response.result.value {
            let json = JSON(jsonData)
            //NSLog("ApiController:Enlaces:Response:JSON: \(json)")
            //print(jsonData)
            
            let posts = json
            for index in 0..<(posts.count) {
              let jsonPost = posts.arrayValue[index]
              
              var emailAutor: String!
              if let e = jsonPost[Constants.Depoimentos.Props.emailAutor].string {
                emailAutor = e
              }
              
              var textoDepoimento: String!
              if let d = jsonPost[Constants.Depoimentos.Props.textoDepoimento].string {
                textoDepoimento = d
              }
              
              var ativo: Bool!
              if let a = jsonPost[Constants.Depoimentos.Props.ativo].bool {
                ativo = a
              }
              
              let depoimento = Depoimento(
                textoDepoimentoParam: textoDepoimento,
                emailAutorParam: emailAutor,
                ativoParam: ativo
              )
              depoimentos.append(depoimento)
            }
            completionHandler(depoimentos, nil)
          }
        case .failure(let error):
          //NSLog("ApiController:DoLogin:LoginPageViewController: Error: \(error) ")
          if let headersData = response.response?.allHeaderFields {
            let headers = JSON(headersData);
            if let message = headers["message"].string {
              
              let errorReportData: NSMutableDictionary = NSMutableDictionary()
              errorReportData.setValue(statusCode, forKey: "statusCode")
              errorReportData.setValue(message, forKey:"message")
              errorReportData.setValue(error, forKey:"error")
              completionHandler(nil, errorReportData)
            }
            else
            {
              let errorReportData: NSMutableDictionary = NSMutableDictionary()
              errorReportData.setValue(statusCode, forKey: "statusCode")
              errorReportData.setValue("", forKey:"message")
              errorReportData.setValue(error, forKey:"error")
              completionHandler(nil, errorReportData)
            }
          } // end of failure case.
        } // end of switch
      }
  }
}

  
  
  /**
   @function: doGetTipoProcedimentos
   @abstract: TipoProcedimentos posts related to MyClin.cca from the API
   @author about.me/mmmattos
   */
  class func doGetTipoProcedimentos(
    completionHandler: @escaping ([TipoProcedimento]?, NSMutableDictionary?) -> ()
    ) -> ()
  {
    
    var tipoProcedimentos: [TipoProcedimento] = []
    
    SessionManager.default.request(Router.GetTipoProcedimentos())
      .validate(statusCode: 200..<300)
      .responseJSON { response in
        if (response.response?.statusCode) != nil {
          let statusCode = response.response?.statusCode
          switch response.result {
          case .success:
            if let jsonData = response.result.value {
              let json = JSON(jsonData)
              //NSLog("ApiController:TipoProcedimentos:Response:JSON: \(json)")
              //print(jsonData)
              let posts = json
              for index in 0..<(posts.count) {
                let jsonPost = posts.arrayValue[index]
                
                var emailAutor: String!
                if let e = jsonPost[Constants.TipoProcedimentos.Props.emailAutor].string {
                  emailAutor = e
                }
                
                var tipo: String!
                if let t = jsonPost[Constants.TipoProcedimentos.Props.tipo].string {
                  tipo = t
                }
                
                var descricao: String!
                if let d = jsonPost[Constants.TipoProcedimentos.Props.descricao].string {
                  descricao = d
                }
                
                var ativo: Bool!
                if let a = jsonPost[Constants.TipoProcedimentos.Props.ativo].bool {
                  ativo = a
                }
                
                let tipoProcedimento = TipoProcedimento(
                  tipoParam: tipo,
                  descricaoParam: descricao,
                  emailAutorParam: emailAutor,
                  ativoParam: ativo
                )
                tipoProcedimentos.append(tipoProcedimento)
              }
              completionHandler(tipoProcedimentos, nil)
            }
          case .failure(let error):
            if let headersData = response.response?.allHeaderFields {
              let headers = JSON(headersData);
              if let message = headers["message"].string {
                
                let errorReportData: NSMutableDictionary = NSMutableDictionary()
                errorReportData.setValue(statusCode, forKey: "statusCode")
                errorReportData.setValue(message, forKey:"message")
                errorReportData.setValue(error, forKey:"error")
                completionHandler(nil, errorReportData)
              }
              else
              {
                let errorReportData: NSMutableDictionary = NSMutableDictionary()
                errorReportData.setValue(statusCode, forKey: "statusCode")
                errorReportData.setValue("", forKey:"message")
                errorReportData.setValue(error, forKey:"error")
                completionHandler(nil, errorReportData)
              }
            } // end of failure case.
          } // end of switch
        }
    }
  }
  

  
  
  /**
   @function: doGetTipoProcedimentosByTipo
   @abstract: TipoProcedimentos posts for a given "tipo" related to MyClin.cca from the API
   @author about.me/mmmattos
   */
  class func doGetTipoProcedimentosByTipo(
    tipo: String,
    completionHandler: @escaping (TipoProcedimento?, NSMutableDictionary?) -> ()
    ) -> ()
  {
    SessionManager.default.request(Router.GetTipoProcedimentosByTipo(tipo))
      .validate(statusCode: 200..<300)
      .responseJSON { response in
        if (response.response?.statusCode) != nil {
          let statusCode = response.response?.statusCode
          switch response.result {
          case .success:
            if let jsonData = response.result.value {
              let json = JSON(jsonData)
              let posts = json
              let jsonPost = posts.arrayValue[0]
              
              var emailAutor: String!
              if let e = jsonPost[Constants.TipoProcedimentos.Props.emailAutor].string {
                emailAutor = e
              }
              
              var tipo: String!
              if let t = jsonPost[Constants.TipoProcedimentos.Props.tipo].string {
                tipo = t
              }
              
              var descricao: String!
              if let d = jsonPost[Constants.TipoProcedimentos.Props.descricao].string {
                descricao = d
              }
              
              var ativo: Bool!
              if let a = jsonPost[Constants.TipoProcedimentos.Props.ativo].bool {
                ativo = a
              }
              
              let tipoProcedimento = TipoProcedimento(
                tipoParam: tipo,
                descricaoParam: descricao,
                emailAutorParam: emailAutor,
                ativoParam: ativo
              )
              completionHandler(tipoProcedimento, nil)
            }
          case .failure(let error):
            if let headersData = response.response?.allHeaderFields {
              let headers = JSON(headersData);
              if let message = headers["message"].string {
                
                let errorReportData: NSMutableDictionary = NSMutableDictionary()
                errorReportData.setValue(statusCode, forKey: "statusCode")
                errorReportData.setValue(message, forKey:"message")
                errorReportData.setValue(error, forKey:"error")
                completionHandler(nil, errorReportData)
              }
              else
              {
                let errorReportData: NSMutableDictionary = NSMutableDictionary()
                errorReportData.setValue(statusCode, forKey: "statusCode")
                errorReportData.setValue("", forKey:"message")
                errorReportData.setValue(error, forKey:"error")
                completionHandler(nil, errorReportData)
              }
            } // end of failure case.
          } // end of switch
        }
    }
  }



/**
 @function: doGetDuvida
 @abstract: Dúvidas posts related to MyClin.cca from the API
 @author about.me/mmmattos
 */
class func doGetDuvidas(
  completionHandler: @escaping ([Duvida]?, NSMutableDictionary?) -> ()
  ) -> ()
{
  
  var duvidas: [Duvida] = []
  
  SessionManager.default.request(Router.GetDuvidas())
    .validate(statusCode: 200..<300)
    .responseJSON { response in
      if (response.response?.statusCode) != nil {
        let statusCode = response.response?.statusCode
        switch response.result {
        case .success:
          if let jsonData = response.result.value {
            let json = JSON(jsonData)
            //NSLog("ApiController:Enlaces:Response:JSON: \(json)")
            //print(jsonData)
            
            let posts = json
            for index in 0..<(posts.count) {
              let jsonPost = posts.arrayValue[index]
              
              var resposta: String = "Ainda sem resposta..."
              if let r = jsonPost[Constants.Duvidas.Props.resposta].string {
                resposta = r
              }
              
              var pergunta: String!
              if let p = jsonPost[Constants.Duvidas.Props.pergunta].string {
                pergunta = p
              }
              
              var ativo: Bool!
              if let a = jsonPost[Constants.Duvidas.Props.ativo].bool {
                ativo = a
              }
              
              let duvida = Duvida(
                perguntaParam: pergunta,
                respostaParam: resposta,
                ativoParam: ativo
              )
              duvidas.append(duvida)
            }
            completionHandler(duvidas, nil)
          }
        case .failure(let error):
          //NSLog("ApiController:DoLogin:LoginPageViewController: Error: \(error) ")
          if let headersData = response.response?.allHeaderFields {
            let headers = JSON(headersData);
            if let message = headers["message"].string {
              
              let errorReportData: NSMutableDictionary = NSMutableDictionary()
              errorReportData.setValue(statusCode, forKey: "statusCode")
              errorReportData.setValue(message, forKey:"message")
              errorReportData.setValue(error, forKey:"error")
              completionHandler(nil, errorReportData)
            }
            else
            {
              let errorReportData: NSMutableDictionary = NSMutableDictionary()
              errorReportData.setValue(statusCode, forKey: "statusCode")
              errorReportData.setValue("", forKey:"message")
              errorReportData.setValue(error, forKey:"error")
              completionHandler(nil, errorReportData)
            }
          } // end of failure case.
        } // end of switch
      }
  }
}



/**
 @function: doGetEnlace
 @abstract: Load Linka related to MyClin.cca from the API
 @author about.me/mmmattos
 */
class func doGetEnlaces(
  completionHandler: @escaping ([Enlace]?, NSMutableDictionary?) -> ()
  ) -> ()
{
  
  var enlaces: [Enlace] = []
  
  SessionManager.default.request(Router.GetEnlaces())
    .validate(statusCode: 200..<300)
    .responseJSON { response in
      if (response.response?.statusCode) != nil {
        let statusCode = response.response?.statusCode
        switch response.result {
        case .success:
          if let jsonData = response.result.value {
            let json = JSON(jsonData)
            //NSLog("ApiController:Enlaces:Response:JSON: \(json)")
            //print(jsonData)
            
            let posts = json
            for index in 0..<(posts.count) {
              let jsonPost = posts.arrayValue[index]
              
              var titulo: String!
              if let t = jsonPost[Constants.Enlaces.Props.titulo].string {
                titulo = t
              }
              
              var url: String!
              if let u = jsonPost[Constants.Enlaces.Props.url].string {
                url = u
              }
              
              var ativo: Bool!
              if let a = jsonPost[Constants.Enlaces.Props.ativo].bool {
                ativo = a
              }
              
              let enlace = Enlace(
                urlParam: url,
                tituloParam: titulo,
                ativoParam: ativo
              )
              enlaces.append(enlace)
            }
            completionHandler(enlaces, nil)
          }
        case .failure(let error):
          //NSLog("ApiController:DoLogin:LoginPageViewController: Error: \(error) ")
          if let headersData = response.response?.allHeaderFields {
            let headers = JSON(headersData);
            if let message = headers["message"].string {
              
              let errorReportData: NSMutableDictionary = NSMutableDictionary()
              errorReportData.setValue(statusCode, forKey: "statusCode")
              errorReportData.setValue(message, forKey:"message")
              errorReportData.setValue(error, forKey:"error")
              completionHandler(nil, errorReportData)
            }
            else
            {
              let errorReportData: NSMutableDictionary = NSMutableDictionary()
              errorReportData.setValue(statusCode, forKey: "statusCode")
              errorReportData.setValue("", forKey:"message")
              errorReportData.setValue(error, forKey:"error")
              completionHandler(nil, errorReportData)
            }
          } // end of failure case.
        } // end of switch
      }
  }
}



/**
 @function: doGetSecoesClinica
 @abstract: Load secoes da Clinica view from the API
 @author about.me/mmmattos
 */
class func doGetSecoesClinica(
  completionHandler: @escaping ([ClinicaSection]?, NSMutableDictionary?) -> ()
  ) -> ()
{
  
  var clinicaSections: [ClinicaSection] = []
  
  SessionManager.default.request(Router.SecoesClinica())
    .validate(statusCode: 200..<300)
    .responseJSON { response in
      if (response.response?.statusCode) != nil {
        let statusCode = response.response?.statusCode
        switch response.result {
        case .success:
          if let jsonData = response.result.value {
            let json = JSON(jsonData)
            //NSLog("ApiController:DoLogin:Response:JSON: \(response) ")
            //NSLog("ApiController:SecoesClinica:Response:JSON: \(json)")
            //print(jsonData)
            
            let posts = json
            for index in 0..<(posts.count) {
              let jsonPost = posts.arrayValue[index]
              
              var secaoId: String!
              if let sId = jsonPost[Constants.ClinicaSection.Props.secaoId].string {
                secaoId = sId
              }
              
              
              var secao: String!
              if let s = jsonPost[Constants.ClinicaSection.Props.secao].string {
                secao = s
              }
              
              var emailAutor: String!
              if let eA = jsonPost[Constants.ClinicaSection.Props.emailAutor].string {
                emailAutor = eA
              }
              
              var ordem: Int16!
              if let o = jsonPost[Constants.ClinicaSection.Props.ordem].int16 {
                ordem = o
              }
              
              var tipo: String!
              if let t = jsonPost[Constants.ClinicaSection.Props.tipo].string {
                tipo = t
              }
              
              
              var ativo: Bool!
              if let a = jsonPost[Constants.ClinicaSection.Props.ativo].bool {
                ativo = a
              }
              
              
              var conteudo: String!
              if let c = jsonPost[Constants.ClinicaSection.Props.conteudo].string {
                conteudo = c
              
                let clinicaSection = ClinicaSection(
                  tipoParam: tipo,
                  conteudoParam: conteudo,
                  ordemParam: ordem,
                  emailAutorParam: emailAutor,
                  ativoParam: ativo,
                  secaoIdParam: secaoId,
                  secaoParam: secao
                )
                // Add the post to posts
                clinicaSections.append(clinicaSection)
              
              }
            }
            completionHandler(clinicaSections, nil)
          }
        case .failure(let error):
          //NSLog("ApiController:DoLogin:LoginPageViewController: Error: \(error) ")
          if let headersData = response.response?.allHeaderFields {
            let headers = JSON(headersData);
            if let message = headers["message"].string {
              
              let errorReportData: NSMutableDictionary = NSMutableDictionary()
              errorReportData.setValue(statusCode, forKey: "statusCode")
              errorReportData.setValue(message, forKey:"message")
              errorReportData.setValue(error, forKey:"error")
              completionHandler(nil, errorReportData)
            }
            else
            {
              let errorReportData: NSMutableDictionary = NSMutableDictionary()
              errorReportData.setValue(statusCode, forKey: "statusCode")
              errorReportData.setValue("", forKey:"message")
              errorReportData.setValue(error, forKey:"error")
              completionHandler(nil, errorReportData)
            }
          } // end of failure case.
        } // end of switch
      }
  }
}


/**
 Save Story to the API
 @param RichTextStoryContent
 @returns status
 */
class func doCreateAppointment (
  descricao: String,
  tipo: String,
  isPublished: Bool,
  completionHandler: @escaping (String?, Error?) -> ()) -> () {
  
  //        var post: BaseContentStory!
  //
  //        switch postType {
  //        case Constants.ContentTypes.OneLine.id:
  //                post = OneLineContentStory(
  //                    storyTitleParam: "Tweet-like Post",
  //                    storyDetailsParam: "Details placeholder",
  //                    storyTextParam: postText,
  //                    storyFolderGroupParam: "Default",
  //                    storyTagsParam: "tag1, tag2",
  //                    storyIsPublishedParam: isPublished
  //
  //                )
  //                break
  //
  //            case Constants.ContentTypes.RichText.id:
  //                post = RichTextStoryContent (
  //                    storyTitleParam: "Rich Text Post",
  //                    storyDetailsParam: "Details placeholder",
  //                    storyTextParam: postText,
  //                    storyFolderGroupParam: "Default",
  //                    storyTagsParam: "tag1, tag2",
  //                    storyIsPublishedParam: isPublished
  //                )
  //                break
  //
  //            case Constants.ContentTypes.Images.id: break
  //            case Constants.ContentTypes.Video.id: break
  //            case Constants.ContentTypes.Audio.id: break
  //            default: break
  //
  //        }
  //
  //        SessionManager.default.request(
  //            Router.CreatePost(
  //                jsonResponse(
  //                    data: post.toJson()!
  //                )
  //            )
  //        )
  //        .validate(statusCode: 200..<300)
  //        .responseJSON { response in
  //            switch response.result {
  //            case .success:
  //                if let jsonData = response.result.value {
  //                    let json = JSON(jsonData)
  //
  //                    Utilities.Log(
  //                        message:"RichTextPost Response JSON: \(json)",
  //                        Utilities.typeName(some: self),
  //                        #file,
  //                        #function,
  //                        #line
  //                    )
  //
  //                    let str = json["message"].string
  //                    completionHandler(str, nil)
  //
  //                }
  //
  //            case .failure(let error):
  //                Utilities.Log(
  //                    message:"RichTextPost Response Error: \(error)",
  //                    Utilities.typeName(some:self),
  //                    #file,
  //                    #function,
  //                    #line
  //                )
  //                completionHandler(nil, error)
  //            }
  //        }
}
/**
 class func doGetMockedContents (
 completionHandler: @escaping (
 [BaseContentStory]?, Error?
 ) -> ()) -> () {
 
 var mockedPosts: [BaseContentStory] = []
 
 SessionManager.default.request(
 Router.GetMockedContents()
 )
 .validate(statusCode: 200..<300)
 .responseJSON { response in
 switch response.result {
 case .success:
 
 if let jsonData = response.result.value {
 let json = JSON(jsonData)
 //let json = JSON(data: jsonData as! Data)
 Utilities.Log(
 message:"RichTextPost Response JSON: \(json)",
 Utilities.typeName(some: self),
 #file,
 #function,
 #line
 )
 
 
 
 let posts = json["contents"]
 //print(posts)
 for index in 0..<(posts.count) {
 
 let jsonPost = posts.arrayValue[index]
 
 var storyId: String!
 if let sI = jsonPost[storyIdAttr].string {
 storyId = sI
 }
 
 var storyTitle: String!
 if let tI = jsonPost[storyTitleAttr].string {
 storyTitle = tI
 }
 
 var storyDetails: String!
 if let sD = jsonPost[storyDetailsAttr].string {
 storyDetails = sD
 }
 
 
 var storyText: String!
 if let tX = jsonPost[storyTextAttr].string {
 storyText = tX
 }
 
 var storyAuthorId: String!
 if let ai = jsonPost[storyAuthorIdAttr].string {
 storyAuthorId = ai
 }
 
 
 var storyGroupFolder: String!
 if let sGf = jsonPost[storyGroupFolderAttr].string {
 storyGroupFolder = sGf
 }
 
 
 var storyType: String!
 if let sTy = jsonPost[storyTypeAttr].string {
 storyType = sTy
 }
 
 
 var storyIsPublished:Bool!
 if let ip = jsonPost[storyIsPublishedAttr].bool {
 storyIsPublished = ip
 }
 
 
 var storyIsPaid:Bool!
 if let iPaid = jsonPost[storyIsPaidAttr].bool {
 storyIsPaid = iPaid
 }
 
 
 var storyTags: String = ""
 if let sT = jsonPost[storyTagsAttr].arrayObject {
 storyTags = (sT as! [String]).joined(separator: ", ")
 }
 
 
 var storyCreationDate: Date!
 let cd = jsonPost[storyCreationDateAttr].double
 if let theDate = Date(jsonDate: cd) {
 storyCreationDate = theDate
 }
 
 var storySavedDate: Date!
 let sd = jsonPost[storySavedDateAttr].double
 if let theSDate = Date(jsonDate: sd) {
 storySavedDate = theSDate
 }
 
 //                        print("Story ID: \(storyId)")
 //                        print("Story Title: \(storyTitle)")
 //                        print("Story Details: \(storyDetails)")
 //                        print("Story Text: \(storyText)")
 //                        print("Story Tags: \(storyTags)")
 //                        print("Story Author ID: \(storyAuthorId)")
 //                        print("Story Creation Date: \(storyCreationDate)")
 //                        print("Story Saved Date: \(storySavedDate)")
 //                        print("Story Is Published: \(storyIsPublished)")
 //                        print("Story Is Paid: \(storyIsPaid)")
 
 let mockedPost = BaseContentStory(
 storyTitleParam: storyTitle,
 storyDetailsParam: storyDetails,
 storyTextParam: storyText,
 storyTypeParam: storyType,
 storyFolderGroupParam: storyGroupFolder,
 storyTagsParam: storyTags,
 storyIsPublishedParam: storyIsPublished,
 storyAuthorIdParam: storyAuthorId,
 storyCreationDateParam: storyCreationDate,
 storySavedDateParam: storySavedDate,
 storyIsPaidParam: storyIsPaid,
 storyIdParam: storyId)
 
 // print ("Mocked BaseContentStory: \(
 
 // Add the post to posts
 mockedPosts.append(mockedPost)
 
 }
 
 completionHandler(mockedPosts, nil)
 
 }
 
 case .failure(let error):
 Utilities.Log(
 message:"RichTextPost Response Error: \(error)",
 Utilities.typeName(some:self),
 #file,
 #function,
 #line
 )
 completionHandler(nil, error)
 }
 }
 }
 **/

/**
 Save the marker by calling the API
 @param descriptionText  String Place description.
 @param latitude:    The marker latitude.
 @param longitude:   The marker longitude.
 @return
 @author about.me/mmmattos
 */
//    class func doSaveMarker
//        (
//        descriptionText: String,
//        latitude: Double,
//        longitude: Double,
//        completionHandler: (String?, ErrorType?) -> ()) -> ()
//    {
//
//        let marker = Marker(
//            descricao: descriptionText,
//            lat: latitude,
//            long: longitude)
//
//        Manager.sharedInstance.request(
//            Router.SaveMarker(jsonResponse(marker.toJson()!))
//        ).responseSwiftyJSON ({
//            (request, response, json, error) in
//            let str = json["message"].string
//            completionHandler(str, error)
//        })
//
//    }


/**
 @function: doLoadMarkers
 @abstract: Load markers from the API
 @param latitude:    The marker latitude.
 @param longitude:   The marker longitude.
 @author about.me/mmmattos
 */
//    class func doLoadMarkers(
//        latitude: Double,
//        longitude: Double,
//        completionHandler: (SwiftyJSON.JSON?, ErrorType?) -> ()) -> ()
//    {
//        let marker = Marker(
//            descricao: "dummy",
//            lat: latitude,
//            long: longitude
//        )
//
//
//        Manager.sharedInstance.request(
//            Router.Markers(jsonResponse(marker.toJson()!))
//            ).responseSwiftyJSON ({
//                (request, response, json, error) in
//                //println(json)
//                completionHandler(json, error)
//            })
//    }




/**
 Converts JSON Object NSData into an NSDictionary [String: AnyObject]
 @param data An NSData with the Json Object NSData to be converted
 @returns [String: AnyObject]
 @author about.me/mmmattos
 */
class func jsonResponse(data: NSData) -> [String : AnyObject]
{
  do {
    let json: AnyObject! = try JSONSerialization.jsonObject(
      with: data as Data,
      options: JSONSerialization.ReadingOptions.mutableContainers
      //,error: nil
      
      ) as AnyObject!
    return json as! [String : AnyObject]
  }
  catch let error as NSError {
    print("ERROR: Unable to convert json object data into dict, error: \(error)")
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CrashlyticsLogNotification"), object: self)
    
    
    return error as! [String : AnyObject]
  }
}



//    class func getMarkersOld() {
//        Alamofire.request(
//            .GET,
//            "http://ballam-shaved.codio.io:3000/api/markers",
//            parameters: ["username": "miguel","password":"12345"])
//            .responseSwiftyJSON { (request, response, json, error) in
//                //println(request)
//                //println(response)
//                println(json)
//                println(error)
//        }
//    }
}
