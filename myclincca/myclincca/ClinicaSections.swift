//
//  ClinicaSections.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 14/04/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import Foundation
import SwiftyJSON

public class ClinicaSection : Serializable {
  var secaoId: String?
  var secao: String?
  var tipo: String!
  var conteudo: String!
  var ordem: Int16!
  var emailAutor: String!
  var ativo: Bool
  
  init(

    tipoParam: String,
    conteudoParam: String,
    ordemParam: Int16,
    emailAutorParam: String,
    ativoParam: Bool,
    secaoIdParam: String?,
    secaoParam: String?
  ) {

    self.tipo = tipoParam
    self.conteudo = conteudoParam
    self.ordem = ordemParam
    self.emailAutor = emailAutorParam
    self.ativo = ativoParam
    
    
    if let sId = secaoIdParam {
      self.secaoId = sId
    }
    
    if let s = secaoParam {
        self.secao = s
    }
  }
  
  func showMe() -> String? {
    var result = "===========================================\n"
    result = result + "Secão \(String(describing: self.secao))\n"
    result = result + "ID    \(String(describing: self.secaoId))\n"
    result = result + "Tipo  \(self.tipo)\n"
    result = result + "Cont. \(self.conteudo)\n"
    result = result + "Ordem \(self.ordem)\n"
    result = result + "Ativo \(self.ativo)\n"
    result = result + "Email \(self.emailAutor)\n"
    result = result + "===========================================\n"
    return result
  }
  
}
