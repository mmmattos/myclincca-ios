//
//  DateFormatterExtension.swift
//  myclincca
//
//  Usage:
//  let dateToString = "2016-12-31T23:59:59.9999999"
//  let dateTo = DateFormatter.date(fromISO8601StringWithOptionalMilliseconds: dateToString)
//  // dateTo: 2016-12-31 23:59:59 +0000
//
//  let dateFromString = "2016-12-01T00:00:00"
//  let dateFrom = DateFormatter.date(fromISO8601StringWithOptionalMilliseconds: dateFromString)
//  // dateFrom: 2016-12-01 00:00:00 +0000
//
//
//  Created by Miguel Miranda de Mattos on 14/04/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import Foundation

extension DateFormatter {
  
  static let iso8601DateFormatter: DateFormatter = {
    let enUSPOSIXLocale = Locale(identifier: "en_US_POSIX")
    let iso8601DateFormatter = DateFormatter()
    iso8601DateFormatter.locale = enUSPOSIXLocale
    iso8601DateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    iso8601DateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
    return iso8601DateFormatter
  }()
  
  static let iso8601WithoutMillisecondsDateFormatter: DateFormatter = {
    let enUSPOSIXLocale = Locale(identifier: "en_US_POSIX")
    let iso8601DateFormatter = DateFormatter()
    iso8601DateFormatter.locale = enUSPOSIXLocale
    iso8601DateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    iso8601DateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
    return iso8601DateFormatter
  }()
  
  static func date(fromISO8601StringWithOptionalMilliseconds string: String) -> Date? {
    if let dateWithMilliseconds = iso8601DateFormatter.date(from: string) {
      return dateWithMilliseconds
    }
    
    if let dateWithoutMilliseconds = iso8601WithoutMillisecondsDateFormatter.date(from: string) {
      return dateWithoutMilliseconds
    }
    
    return nil
  }
}
