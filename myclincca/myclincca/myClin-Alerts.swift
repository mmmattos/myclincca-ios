//
//  Enceen-Alerts.swift
//  Enceen.iOS
//
//  Created by Miguel Miranda de Mattos.
//
import UIKit

// Notes:
// - You must explicitly weak link to UIKit to support iOS 7
// - Action sheets on iOS 7 only show a title

class mmmattosAlerts {
    
    class func showAlert(
        viewController: UIViewController,
        style: UIAlertControllerStyle = .alert,
        title: String? = nil,
        message: String? = nil,
        sourceView: UIView? = nil,
        completion: (() -> ())? = nil,
        buttons: (UIAlertActionStyle, String, (() -> ())?)...)
    {
            
            if (NSClassFromString("UIAlertController") != nil) {
                // iOS 8+
                let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
                for button in buttons {
                    alertController.addAction(UIAlertAction(title: button.1, style: button.0) { (_: UIAlertAction!) in
                        if let completion = completion { completion() }
                        if let cb = button.2 { cb() }
                        })
                }
                if let sourceView = sourceView {
                    if let popoverPresentationController = alertController.popoverPresentationController {
                        popoverPresentationController.sourceView = sourceView
                        popoverPresentationController.sourceRect = sourceView.bounds
                    }
                }
                
                viewController.present(alertController, animated: true, completion: nil)
                
            } else {
                // iOS 7
                class Helper: NSObject {
                    
                    var holdSelf: Helper?
                    var cb: (Int) -> ()
                    
                    init(cb: @escaping (Int) -> ()) {
                        self.cb = cb
                        super.init()
                        self.holdSelf = self
                    }
                    
                    func finish(index: Int) {
                        self.cb(index)
                        self.holdSelf = nil
                    }
                }
                
                switch style {
                case .actionSheet:
                    class ActionSheetHelper : Helper, UIActionSheetDelegate {
                        init(_ actionSheet: UIActionSheet, cb: @escaping (Int) -> ()) {
                            super.init(cb: cb)
                            actionSheet.delegate = self
                            
                        }
                        func actionSheet(actionSheet: UIActionSheet!, clickedButtonAtIndex buttonIndex: Int) {
                            actionSheet.delegate = nil
                            self.finish(index: buttonIndex)
                        }
                    }
                    let actionSheet = UIActionSheet()
                    if let title = title {
                        actionSheet.title = title
                    }
                    for button in buttons {
                        actionSheet.addButton(withTitle: button.1)
                        switch button.0 {
                        case .default:
                            break
                        case .cancel:
                            actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1
                        case .destructive:
                            actionSheet.destructiveButtonIndex = actionSheet.numberOfButtons - 1
                        }
                    }
                    ActionSheetHelper(actionSheet) {
                        if let completion = completion { completion() }
                        if let cb = buttons[$0].2 { cb() }
                    }
                    if let sourceView = sourceView {
                        actionSheet.show(from: sourceView.bounds, in: sourceView, animated: true)
                    } else {
                        actionSheet.show(in: viewController.view)
                    }
                case .alert:
                    class AlertHelper : Helper, UIAlertViewDelegate {
                        init(_ alertView: UIAlertView, cb: @escaping (Int) -> ()) {
                            super.init(cb: cb)
                            alertView.delegate = self
                            
                        }
                        func alertView(alertView: UIAlertView!, clickedButtonAtIndex buttonIndex: Int) {
                            alertView.delegate = nil
                            self.finish(index: buttonIndex)
                        }
                    }
                    let alertView = UIAlertView()
                    if let title = title {
                        alertView.title = title
                    }
                    if let message = message {
                        alertView.message = message
                    }
                    for button in buttons {
                        alertView.addButton(withTitle: button.1)
                        switch button.0 {
                        case .cancel:
                            alertView.cancelButtonIndex = alertView.numberOfButtons - 1
                        default:
                            break
                        }
                    }
                    AlertHelper(alertView) {
                        if let completion = completion { completion() }
                        if let cb = buttons[$0].2 { cb() }
                    }
                    alertView.show()
                }
            }
    }
}
