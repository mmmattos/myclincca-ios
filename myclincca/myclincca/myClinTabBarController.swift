//
//  enceenTabBarController.swift
//  enceenApp10
//
//  Created by Miguel Miranda de Mattos on 26/11/16.
//  Copyright © 2016 Miranda Mattos Informatica Ltda. All rights reserved.
//

import UIKit

class enceenTabBarController: UITabBarController {

    @IBInspectable var defaultIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        selectedIndex = defaultIndex

        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print ("Selected tabbaritem \(item.tag)")

        switch item.tag
        {
          case 0, 1, 2: break   // TODO.
          default: break
          
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
