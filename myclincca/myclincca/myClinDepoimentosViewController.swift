//
//  myClinDepoimentosViewController.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 10/07/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import UIKit

class myClinDepoimentosViewController: UIViewController {
  
  @IBOutlet weak var depoimentosView: UIScrollView!
  
  @IBOutlet weak var backButton: UIButton!
  
  @IBAction func backButtonAction(_ sender: UIButton) {
    
    self.dismiss(animated: true)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    
    depoimentosView.isScrollEnabled = true
    
    loadDepoimentos() {
      (items,  error) in
      if let depoimentos = items {
        Utilities.Log(
          message: "Encontrei Depoimentos a renderizar: \(depoimentos)",
          #file,
          Utilities.typeName(some: self),
          #function,
          #line
        )
        self.renderDepoimentos(depoimentosToRender: depoimentos)
      }
    }
    
  }
  
  var fullHeight: Double = 0
  
  func createCustomLabel(text: String, tipo: String, positionY: Double, nbrLines: Int, fontName: String, fontSize: Double, tag: Int ) {
    let initialFrame = CGRect(x: CGFloat(0), y: CGFloat(positionY), width: depoimentosView.frame.width , height: CGFloat(44))
    let contentInsets = UIEdgeInsets(top: 20, left: 20, bottom: 10, right: 10)
    let paddedFrame = UIEdgeInsetsInsetRect(initialFrame, contentInsets)
    let codedLabel:UILabel = UILabel(frame: paddedFrame)

    codedLabel.numberOfLines = nbrLines
    codedLabel.contentMode = .center
    codedLabel.text = text
    codedLabel.tag = tag
    switch tipo.lowercased() {
    case "email" :
      codedLabel.font=UIFont.init(name: fontName, size: CGFloat(fontSize))
      codedLabel.textAlignment = .right
      codedLabel.textColor=UIColor.blue
      codedLabel.backgroundColor = UIColor.clear
      codedLabel.numberOfLines=text.characters.count/20
    case "depoimento" :
      codedLabel.font=UIFont.init(name: fontName, size: CGFloat(fontSize))
      codedLabel.textAlignment = .justified
      codedLabel.textColor = UIColor.darkGray
      codedLabel.backgroundColor = UIColor.clear
      codedLabel.numberOfLines=text.characters.count/20
    default :
      codedLabel.textAlignment = .center
      
    }

    codedLabel.lineBreakMode = .byWordWrapping;
    codedLabel.sizeToFit()
    self.depoimentosView.addSubview(codedLabel)
    print ("Rendered Label: \(String(describing: codedLabel.text))")
    fullHeight = fullHeight + Double(codedLabel.frame.height)
    
    
  }
  
  
  func renderDepoimentos( depoimentosToRender: [Depoimento]) {
    var inativos = 0
    
    for x:Int in 0..<depoimentosToRender.count {
      let depoimento = depoimentosToRender[x]
      
      if !depoimento.ativo {
        inativos += 1
        continue
      }
      
      if depoimento.textoDepoimento != nil {
        createCustomLabel(
          text: depoimento.textoDepoimento!,
          tipo: "depoimento",
          positionY: Double(self.fullHeight),
          nbrLines: 2,
          fontName: "HelveticaNeue-CondensedBold",
          fontSize: 20,
          tag: x
        )
        self.fullHeight = self.fullHeight + 10
      }
      
      if depoimento.emailAutor != nil {
        createCustomLabel(
          text: depoimento.emailAutor!,
          tipo: "email",
          positionY: Double(self.fullHeight),
          nbrLines: 2,
          fontName: "HelveticaNeue-CondensedBold",
          fontSize: 20,
          tag: x
        )
        self.fullHeight = self.fullHeight + 40
      }
    }
    if depoimentosToRender.count == 0 || depoimentosToRender.count == inativos  {
      createCustomLabel(
        text: "Não há Depoimentos disponíveis para Consulta. Tente mais tarde.",
        tipo: "mansagem",
        positionY: Double(self.fullHeight),
        nbrLines: 5,
        fontName: "HelveticaNeue-CondensedBlack",
        fontSize: 20,
        tag: 0
      )
      return
    }
    print ("Full Height is: \(self.fullHeight)")
    let screenSizeWidth = UIScreen.main.bounds.size.width
    depoimentosView.contentSize = CGSize(width: screenSizeWidth, height: CGFloat(self.fullHeight+150))
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  
  func loadDepoimentos(
    completionHandler: @escaping ([Depoimento]?, NSMutableDictionary?) -> ()
    ) -> () {
    
    // MARK: - Read Dúvida
    ApiController.doGetDepoimentos()
      {
        (json, error) in
        if let err = error {
          
          Utilities.Log(
            message: "Error reading Depoimentos! \(err)",
            #file,
            Utilities.typeName(some: self),
            #function,
            #line
          )
          
          let message = "Erro na leitura dos Depoimentos. Por favor tente mais tarde!"
          mmmattosAlerts.showAlert(
            viewController:self as UIViewController,
            style: .alert,
            message: message,
            completion: nil,
            buttons: (.default, "Ok", nil)
          )
          return
        }
        guard let deps = json else {
          
          let message = "Não há Depoimentos disponíveis. Por favor tente mais tarde!"
          
          mmmattosAlerts.showAlert(
            viewController:self as UIViewController,
            style: .alert,
            message: message,
            completion: nil,
            buttons: (.default, "Ok", nil)
          )
          return
        }
        completionHandler(deps, nil)
        
    }
  }
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}
