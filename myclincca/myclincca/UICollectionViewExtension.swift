//
//  UICollectionViewExtension.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 18/04/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView {
  func setItemsInRow(_ items: Int) {
    if let layout = self.collectionViewLayout as? UICollectionViewFlowLayout {
      let contentInset = self.contentInset
      let itemsInRow: CGFloat = CGFloat(items);
      let innerSpace = layout.minimumInteritemSpacing * (itemsInRow - 1.0)
      let insetSpace = contentInset.left + contentInset.right + layout.sectionInset.left + layout.sectionInset.right
      let width = floor((frame.width - insetSpace - innerSpace) / itemsInRow);
      layout.itemSize = CGSize(width: width, height: width)
    }
  }
}
