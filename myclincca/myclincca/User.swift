//
//  User.swift
//  Enceen-IOS
//
//  Created by Miguel Miranda de Mattos.
//

import Foundation
import SwiftyJSON


class User : Serializable {
  var userId:   String?
  var name: String
  var email: String
  var password: String
  var especialidade:String
  var cremers:String
  var telefone:String
  var rg:String
  var cpf:String
  var estadoCivil:String
  var endereco: String
  var cidade: String
  var estado: String
  
  init(
    name:String,
    email:String,
    password:String,
    especialidade:String,
    cremers:String,
    telefone:String,
    rg:String,
    cpf:String,
    estadoCivil: String,
    endereco: String,
    cidade: String,
    estado: String) {
    
    self.userId = nil
    self.name = name
    self.email = email
    self.password = password
    self.especialidade = especialidade
    self.cremers = cremers
    self.telefone = telefone
    self.rg = rg
    self.cpf = cpf
    self.estadoCivil = estadoCivil
    self.endereco = endereco
    self.cidade = cidade
    self.estado = estado
  }
  
  
}
