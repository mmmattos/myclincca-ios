
//
//  RegisterStep2ViewController.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 04/05/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import Foundation
import UIKit
import KeychainAccess


class RegisterStep2ViewController:
  UIViewController
  , UITextFieldDelegate
  , UITextViewDelegate
, UIScrollViewDelegate {
  
  private let SCREEN_SIZE = UIScreen.main.bounds
  private let REGISTER_BOX_HEIGHT_FACTOR:CGFloat = 667.0/230.0
  private let REGISTER_BOX_MIN_HEIGHT:CGFloat = 320.0
  
  var userNome: String = ""
  var userEmail: String = ""
  var userPassword: String = ""
  var userEspecialidade: String = ""
  var userCremers: String = ""
  var userTelefone: String = ""
  var userRg: String = ""
  var userCpf: String = ""
  var userEstadoCivil: String = ""
  var userEndereco: String = ""
  var userCidade: String = ""
  var userEstado: String = ""
  
  @IBOutlet weak var registerStep2Box: UIView!
  @IBOutlet weak var logoMobile: UIImageView!
  @IBOutlet weak var backButton: UIButton!
  
  @IBOutlet weak var estadoTextBox: UITextField!
  
  @IBOutlet weak var cidadeTextBox: UITextField!
  
  @IBOutlet weak var enderecoTextBox: UITextField!
  
  @IBOutlet weak var registerButton: UIButton!
  
  @IBOutlet weak var antButton: UIButton!
  
  
  
  
  @IBOutlet weak var disclaimerLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    registerStep2Box.layer.cornerRadius = 16
    registerStep2Box.layer.masksToBounds = true  // optional
    var desiredHeight :CGFloat = SCREEN_SIZE.height / REGISTER_BOX_HEIGHT_FACTOR
    desiredHeight = (desiredHeight < REGISTER_BOX_MIN_HEIGHT ? REGISTER_BOX_MIN_HEIGHT : desiredHeight)
    let boxY = logoMobile.frame.size.height + 30.0
    registerStep2Box.frame.origin.y = boxY
    registerStep2Box.frame.size.height = desiredHeight
    
    self.enderecoTextBox.delegate = self
    self.cidadeTextBox.delegate = self
    self.estadoTextBox.delegate = self
  }
  
  override func viewDidAppear(_ animated: Bool) {
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    // Restore TEMP data so we can save it if
    let tempRegStore = UserDefaults.standard
    
    // TEMP data 1st screen
    
    if let email = tempRegStore.string(forKey: "register.email") {
      self.userEmail = email
    }
    if let nome = tempRegStore.string(forKey: "register.nome") {
      self.userNome = nome
    }
    if let pw = tempRegStore.string(forKey: "register.pw") {
      self.userPassword = pw
    }
    if let especialidade = tempRegStore.string(forKey: "register.especialidade") {
      self.userEspecialidade = especialidade
    }
    if let cremers = tempRegStore.string(forKey: "register.cremers") {
      self.userCremers = cremers
    }
    
    // TEMP data 2nd screen
    if let cpf = tempRegStore.string(forKey: "register.cpf") {
      self.userCpf = cpf
    }
    if let rg = tempRegStore.string(forKey: "register.rg") {
      self.userRg = rg
    }
    if let telefone = tempRegStore.string(forKey: "register.telefone") {
      self.userTelefone = telefone
    }
    if let estadoCivil = tempRegStore.string(forKey: "register.estado-civil") {
      self.userEstadoCivil = estadoCivil
    }
    
    // TEMP data 3rd screen
    if let endereco = tempRegStore.string(forKey: "register.endereco") {
      self.userEndereco = endereco
      self.enderecoTextBox.text = endereco
    }
    if let cidade = tempRegStore.string(forKey: "register.cidade") {
      self.userCidade = cidade
      self.cidadeTextBox.text = cidade
    }
    if let estado = tempRegStore.string(forKey: "register.estado") {
      self.userEstado = estado
      self.estadoTextBox.text = estado
    }
  }
  
  @IBAction func backButtonAction(_ sender: Any) {
    //self.dismiss(animated: true)
    
    let tempRegStore = UserDefaults.standard
    
    tempRegStore.removeObject(forKey: "register.nome")
    tempRegStore.removeObject(forKey: "register.email")
    tempRegStore.removeObject(forKey: "register.pw")
    tempRegStore.removeObject(forKey: "register.especialidade")
    tempRegStore.removeObject(forKey: "register.cremers")
    tempRegStore.removeObject(forKey: "register.cpf")
    tempRegStore.removeObject(forKey: "register.rg")
    tempRegStore.removeObject(forKey: "register.telefone")
    tempRegStore.removeObject(forKey: "register.estado-civil")
    tempRegStore.removeObject(forKey: "register.endereco")
    tempRegStore.removeObject(forKey: "register.cidade")
    tempRegStore.removeObject(forKey: "register.estado")
    
    //// Perform segue to RegisterStep1VC.
    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    let vc : myClinMenuViewController = mainStoryboard.instantiateViewController(
      withIdentifier: "myclinccaMenu") as! myClinMenuViewController
    self.present(vc, animated: true, completion: nil)
  }
  
  
  @IBAction func antButtonAction(_ sender: Any) {
    let tempRegStore = UserDefaults.standard
    tempRegStore.set(self.enderecoTextBox.text ?? "", forKey: "register.endereco")
    tempRegStore.set(self.cidadeTextBox.text ?? "", forKey: "register.cidade")
    tempRegStore.set(self.estadoTextBox.text ?? "", forKey: "register.estado")
    
    //Perform segue to RegisterStep1VC.
    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    let vc : RegisterStep1ViewController = mainStoryboard.instantiateViewController(
      withIdentifier: "RegisterStep1VC") as! RegisterStep1ViewController
    
    self.present(vc, animated: true, completion: nil)
    
  }
  

  @IBAction func registerButtonAction(_ sender: Any) {
    self.userEndereco = self.enderecoTextBox.text ?? ""
    self.userCidade = self.cidadeTextBox.text ?? ""
    self.userEstado = self.estadoTextBox.text ?? ""
    
    if self.userEndereco.isEmpty || self.userCidade.isEmpty || self.userEstado.isEmpty {
      mmmattosAlerts.showAlert(
        viewController: self as UIViewController,
        style: .alert,
        message: "Todos os campos são obrigatórios!",
        completion: nil,
        buttons: (.default, "Ok", nil)
      )
      return;
    }
    
    // MARK: - register the user
    ApiController.doRegister(
      name: self.userNome,
      email: self.userEmail,
      password: self.userPassword,
      especialidade: self.userEspecialidade,
      cremers: self.userCremers,
      telefone: self.userTelefone,
      rg: self.userRg,
      cpf: self.userCpf,
      estadoCivil: self.userEstadoCivil,
      endereco: self.userEndereco,
      cidade: self.userCidade,
      estado: self.userEstado)
    {
      (str, error) in
      if (str != nil) {
        
        print("Returned form Register API:  \(String(describing: str))")
        mmmattosAlerts.showAlert(
          viewController: self as UIViewController,
          style: .alert,
          message: str!,
          completion: {
            action in
            //self.resetRegisterFields()
            //Perform segue.
            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
            let initialViewController =
              self.storyboard!
              .instantiateViewController(withIdentifier: "myclinccaMenu") as! myClinMenuViewController
            appDelegate.window?.rootViewController = initialViewController
            //initialViewController.currentUserLabel.text = "Olá \(str!)"
            appDelegate.window?.makeKeyAndVisible()
            self.navigationController?.popViewController(
              animated: true
            )
            self.dismiss(animated: true, completion: nil)
            return
        },
          buttons: (.default, "Ok", nil)
        )
        return
      }
      var message = "Ocorreu um erro no registro do usuário. Tente mais tarde!"
      if  error !=  nil {
        
        let statusCode = error!["statusCode"] as! Int
        let errorMessage = error!["message"] as! String
        
        switch statusCode {
        case NSURLErrorCannotFindHost:
          message = "Não foi possível conectar ao servidor de autenticação. Tente mais tarde."
          break
        case NSURLErrorNotConnectedToInternet:
          message = "Conexão com a Internet indisponível"
          break
        case NSURLErrorCannotConnectToHost:
          message = "Não foi possível conectar com o servidor MyClin.cca."
          break
        case 401:
          if errorMessage != "" {
            message = errorMessage
          } else {
            message = "Email ou Senha informados inválidos. Tente novamente."
          }
          break
        case 404:
          if errorMessage != "" {
            message = errorMessage
          } else {
            message = "Usuário não cadastrado"
          }
          break
        case 500:
          message = "Foi detectado um erro não tratado (500)."
          break
        default:
          message = "\(message) (\(statusCode))"
          break
        }
      }
      mmmattosAlerts.showAlert(
        viewController: self as UIViewController,
        style: .alert,
        message: message,
        completion: nil,
        buttons: (.default, "Ok", nil)
      )
      return;
    }
  }
  
  // MARK: - Close Keyboard on Return key.
  func textFieldShouldReturn(_ userText: UITextField) -> Bool {
    userText.resignFirstResponder()
    return true
  }
  
}
