//
//  myClinMenuViewController.swift
//  myclincca
//
//  Created by Miguel Miranda de Mattos on 03/03/17.
//  Copyright © 2017 Miranda Mattos Informatica Ltda. All rights reserved.
//

import UIKit
import KeychainAccess

enum StackViews: String {
  case navegue, clinica, procedimentos, duvidas, depoimentos, links, contato, login
}

class myClinMenuViewController: UIViewController {
  
  let keychain = Keychain(service: Constants.KeychainKeys.KeychainService)
  
  var stackViews: [Int]? = nil
  
  @IBOutlet weak var currentUserLabel: UILabel!
  
  @IBOutlet weak var topMobileHeader: UIImageView!
  
  
  @IBOutlet weak var backPanelImage: UIImageView!
  
  
  @IBOutlet weak var stackMenu: UIStackView!
  
  
  @IBOutlet weak var navegueStackBtn1: UIView!
  
  @IBAction func navegueStackBtn1Action(_ sender: Any) {

    self.backButton.isHidden = false              // MOSTRA BACK BTN
    showHideStackMenuItem( start: 1, finish: 3)   // MOSTRA CLINICA E PROCEDIMENTOS
    showHideStackMenuItem( start: 6, finish: 10 ) // MOSTRA DUV., DEP., LINKS, CONT. E LOGIN.
    showHideStackMenuItem( start: 0, finish: 1)   // OCULTA NAVEGUE.
    if self.currentUserLabel.text == Constants.Messages.UserNotIdentified {
      showHideStackMenuItem(start: 11, finish: 12) // MOSTRA LOGIN
      showHideStackMenuItem(start: 10, finish: 11, forceHide: true) // OCULTA AGENDA
    } else {
      showHideStackMenuItem(start: 11, finish: 12, forceHide: true) // OCULTA LOGIN
      showHideStackMenuItem(start: 10, finish: 11) // MOSTRA AGENDA
    }
  }
  
  @IBOutlet weak var clinicaStackBtn2: UIButton!
  
  
  @IBAction func clinicaStackBtn2Action(_ sender: UIButton) {
  }
  
  @IBOutlet weak var procBtnView: UIView!
  
  
  
  
  
  @IBOutlet weak var procStackBtn3: UIView!
  
  @IBAction func procStackBtn3Action(_ sender: UIButton) {
    showHideStackMenuItem(start: 3, finish: 6)
  }
  
  @IBOutlet weak var procCirurgias: UIButton!
  
  @IBAction func procCirurgiaAction(_ sender: UIButton) {
  }
  
  @IBOutlet weak var procConsultas: UIButton!
  
  @IBAction func procConsultasAction(_ sender: UIButton) {
  }
  
  @IBOutlet weak var procAvaliacoes: UIButton!
  
  @IBAction func procAvaliacoesAction(_ sender: UIButton) {
  }
  
  @IBOutlet weak var duvidasStackBtn4: UIButton!
  
  @IBAction func duvidasStackBtn4Action(_ sender: UIButton) {
  }
  
  @IBOutlet weak var depoimStackBtn5: UIButton!
  
  @IBAction func depoimStackBtn5Action(_ sender: UIButton) {
  }
  
  @IBOutlet weak var linksStackBtn6: UIButton!
  
  @IBAction func linksStackBtn6Action(_ sender: UIButton) {
  }
  
  @IBOutlet weak var contatoStackBtn7: UIButton!
  
  @IBAction func contatoStackBtn7Action(_ sender: UIButton) {
    let email = "myclin.cca@gmail.com"
    if let url = URL(string: "mailto:\(email)") {
      UIApplication.shared.open(url)
    }
  }
  
  @IBOutlet weak var loginStackBtn8: UIButton!
  
  @IBAction func loginStackBtn8Action(_ sender: UIButton) {
  }
  
  @IBOutlet weak var backButton: UIButton!
  
  @IBAction func backButtonAction(_ sender: UIButton) {
    self.backButton.isHidden = true               // OCULTAR BACK BTN
    showHideStackMenuItem( start: 1, finish: 12, forceHide: true)    // OCUTAR TODAS AS OPÇOES
    showHideStackMenuItem( start: 0, finish: 1)     // MOSTRA O NAVEGUE.
  }
  
  
  
  
  
  
  
  
  func showHideStackMenuItem( start: Int, finish: Int, forceHide: Bool = false) {
    //    var startItem = start
    //    var finishItem = finish
    let smSubviews = stackMenu.arrangedSubviews
    //    if forceHide {
    //      startItem = 1
    //      finishItem = 11
    //    }
    for i in start..<finish {
      let itemSv = smSubviews[i]
      if forceHide {
        itemSv.isHidden = forceHide
      } else {
        itemSv.isHidden = !itemSv.isHidden  //hide
      }
    }
  }
  
  
  func tappedBack( sender: AnyObject?)
  {
    
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    //    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
    //    self.navigationController?.navigationBar.shadowImage = UIImage()
    //    self.navigationController?.navigationBar.isTranslucent = true
    //    self.navigationController?.view.backgroundColor = UIColor.clear
    //    self.navigationController?.navigationBar.tintColor = UIColor.white
    //    self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
    //    let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
    //    self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
    
    
    self.currentUserLabel.text = Constants.Messages.UserNotIdentified
    
    let keychain = Keychain(service: Constants.KeychainKeys.KeychainService)
    
    let userDefaults = UserDefaults.standard
    
    // Check if app is run for the first time.
    if userDefaults.bool(forKey: Constants.UserDefaultKeys.HasRunB4Key) == false {
      // Remove Keychain items here
      do {
        if try keychain.contains(Constants.KeychainKeys.SessionTokenKey) || keychain.contains(Constants.KeychainKeys.SessionUserName)  || keychain.contains(Constants.KeychainKeys.SessionUserEmail) {
          if try keychain.contains(Constants.KeychainKeys.SessionTokenKey) {
            Utilities.Log (
              message:"Got session token from previous installations to be deleted!",
              #file,
              Utilities.typeName(some:self),
              #function,
              #line)
            try keychain.remove(Constants.KeychainKeys.SessionTokenKey)
          }
          if try keychain.contains(Constants.KeychainKeys.SessionUserName) {
            Utilities.Log (
              message:"Got session username from previous installations to be deleted!",
              #file,
              Utilities.typeName(some:self),
              #function,
              #line)
            try keychain.remove(Constants.KeychainKeys.SessionUserName)
          }
          if try keychain.contains(Constants.KeychainKeys.SessionUserEmail) {
            Utilities.Log (
              message:"Got session email from previous installations to be deleted!",
              #file,
              Utilities.typeName(some:self),
              #function,
              #line)
            try keychain.remove(Constants.KeychainKeys.SessionUserEmail)
          }
          // Update the flag indicator
          userDefaults.set(true, forKey: Constants.UserDefaultKeys.HasRunB4Key)
          userDefaults.synchronize() // Forces the app to update UserDefaults
        }
      }
      catch let error {
        Utilities.Log(
          message: "\(error) ",
          #file,
          Utilities.typeName(some: self),
          #function,
          #line)
      }
    }
    
    
    // Try to get the current user name
    do {
      if try keychain.contains(Constants.KeychainKeys.SessionUserName) {
        Utilities.Log (
          message:"Will get current user name from keychain!",
          #file,
          Utilities.typeName(some:self),
          #function,
          #line)
        self.currentUserLabel.text = try keychain.get(
          Constants.KeychainKeys.SessionUserName
        )
      }
    }
    catch let error {
      Utilities.Log(
        message: "Error getting current user from keychain: \(error) ",
        #file,
        Utilities.typeName(some: self),
        #function,
        #line)
    }
    
    
    
    
    self.backButton.isHidden = true
    showHideStackMenuItem( start: 1, finish: 12 )
    
    
    
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    if let segueId = segue.identifier {
      var vcTitle = ""
      if segueId == "consultasSegue" ||
         segueId == "cirurgiasSegue" ||
         segueId == "avaliacoesSegue" {
        switch segueId {
          case "consultasSegue":
            vcTitle = "CONSULTAS"
            break
          case "cirurgiasSegue":
            vcTitle = "CIRURGIAS"
            break
          case "avaliacoesSegue":
            vcTitle = "AVALIAÇÕES"
            break
          default:
            break
        }
        (segue.destination as! myClinTipoProcedimentosViewController)
          .vcTipo = vcTitle
      }
    }
    
    //    Get the new view controller using segue.destinationViewController.
    //    Pass the selected object to the new view controller.
   }
}
